#include "ShopScreen.hpp"
#include "Button.hpp"

#include <iostream>

using namespace std;
using namespace sf;

ShopScreen::ShopScreen(RenderWindow * window, Game * game)
    :MenuScreen(window, game)
{


    Button * buttonDamageUp = Button::ButtonDamageUp(window->GetWidth()/2-50,170,150,50, "images/default/upgrade.png", "images/default/upgrade_hover.png");
    Button * buttonLifeUp = Button::ButtonLifeUp(window->GetWidth()/2-50,240,150,50, "images/default/upgrade.png", "images/default/upgrade_hover.png");
    Button * buttonUpFireSpeed = Button::ButtonUpFireSpeed(window->GetWidth()/2-50,310,150,50, "images/default/upgrade.png", "images/default/upgrade_hover.png");
    Button * buttonUpBombPower = Button::ButtonUpBombPower(window->GetWidth()/2-50,380,150,50,  "images/default/upgrade.png", "images/default/upgrade_hover.png");
    Button * buttonAddBomb= Button::ButtonBuyBomb(window->GetWidth()/2-50,450,150,50,  "images/default/upgrade.png", "images/default/upgrade_hover.png");
    Button * buttonContinue = Button::ButtonContinue(window->GetWidth()-200,window->GetHeight()-100,150,50,  "images/default/jouer.png", "images/default/jouer_hover.png");

    MenuScreen::addButton(buttonDamageUp);
    MenuScreen::addButton(buttonLifeUp);
    MenuScreen::addButton(buttonUpFireSpeed);
    MenuScreen::addButton(buttonAddBomb);
    MenuScreen::addButton(buttonUpBombPower);
    MenuScreen::addButton(buttonContinue);

}

ShopScreen::~ShopScreen()
{
    for(sf::String * str : _text)
        delete str;
}


void ShopScreen::draw()
{
    MenuScreen::draw();
    for(sf::String * str : _text)
            _window->Draw(*str);
}

void ShopScreen::update(float fps)
{
    //on supprime tous le texte
    for(sf::String * str : _text)
    {
        delete str;
        _text.erase(str);
    }


    //on les re crée avec les prix actualisés
    sf::String * str = new sf::String("Damage  : Price " + to_string(_game->getPlayer()->getDamageUpgradePrice()), _textFont, 20);
    str->SetPosition(20, 180);
    _text.insert(str);

    str = new sf::String("Health  : Price " + to_string(_game->getPlayer()->getHealthUpgradePrice()), _textFont, 20);
    str->SetPosition(20, 250);
    _text.insert(str);

    str = new sf::String("Fire speed  : Price " + to_string(_game->getPlayer()->getFireRateUpgradePrice()), _textFont, 20);
    str->SetPosition(20, 320);
    _text.insert(str);

    str = new sf::String("Bomb power  : Price " + to_string(_game->getPlayer()->getBombDamageUpgradePrice()), _textFont, 20);
    str->SetPosition(20, 390);
    _text.insert(str);

    str = new sf::String("Bomb  : Price " + to_string(_game->getPlayer()->getBombPrice()), _textFont, 20);
    str->SetPosition(20, 460);
    _text.insert(str);


    MenuScreen::update(fps);
}
