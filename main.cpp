#include <iostream>
#include "GameModel.hpp"
#include "GameView.hpp"
#include "TextView.hpp"
#include "Level.hpp"
#include "MenuScreen.hpp"
#include <time.h>
#include <cstdlib>
#include "Enemy.hpp"
#include "Game.hpp"
#include <SFML/Graphics.hpp>
using namespace std;
using namespace sf;

int main()
{
    RenderWindow * _window = new RenderWindow(sf::VideoMode(GameView::DEF_SCREEN_W, GameView::DEF_SCREEN_H, 32), "SHMUP", sf::Style::Close);
    _window->SetFramerateLimit(60);
    _window->UseVerticalSync(true);
    Game g = Game(_window);

    while(_window->IsOpened() && g.continuePlaying())
    {
        float fps = 1.f/_window->GetFrameTime();
        g.update(fps);
        g.draw();
        g.display();
    }

    return 0;
}
