#include "CreditScreen.hpp"

CreditScreen::CreditScreen(sf::RenderWindow * window, Game * game)
    :MenuScreen(window, game)
{
    sf::String * str = new sf::String("Credits : ", _textFont, 40);
    str->SetPosition(60, 180);
    _credits.insert(str);

    str = new sf::String("Renouleau Sylvain", _textFont, 40);
    str->SetPosition(60, 225);
    _credits.insert(str);

    str = new sf::String("Barbe Benjamin ", _textFont, 40);
    str->SetPosition(60, 270);
    _credits.insert(str);



    Button * buttonMenu = Button::ButtonMainMenu(window->GetWidth()-200,window->GetHeight()-100,150,50,  "images/default/menu.png", "images/default/menu_hover.png");
    MenuScreen::addButton(buttonMenu);

}

CreditScreen::~CreditScreen()
{
    for(sf::String * str : _credits)
        delete str;
}

void CreditScreen::draw()
{
    MenuScreen::draw();
    for(sf::String * str : _credits)
            _window->Draw(*str);
}

void CreditScreen::update(float fps)
{
    MenuScreen::update(fps);
}
