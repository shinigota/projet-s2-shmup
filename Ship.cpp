#include "Ship.hpp"
#include "Bullet.hpp"
#include <iostream>
using namespace std;

Ship::Ship(float x, float y, unsigned int w, unsigned int h, float dx, float dy, int hp)
    :MovableEntity(x, y, w, h, dx, dy), _clockShoot(), _hp(hp), _fireSpeed(DEF_FIRE_SPEED), _lastShoot(-_fireSpeed)
{}

Ship::Ship(float x, float y, float dx, float dy, float hp)
    :MovableEntity(x, y, dx, dy), _hp(hp), _clockShoot(), _fireSpeed(DEF_FIRE_SPEED), _lastShoot(-_fireSpeed)
{}

Ship::Ship()
    :MovableEntity(), _hp(START_HP), _clockShoot(), _fireSpeed(DEF_FIRE_SPEED), _lastShoot(-_fireSpeed)
     {}

Ship::~Ship()
{}

string Ship::toString() const
{
    return "Ship : " + MovableEntity::toString() + " - hp : " + to_string(_hp) + " - fire speed : " + to_string(_fireSpeed) + " - lastShoot : " + to_string(_lastShoot) ;
}

bool Ship::updateHp(int deltaHP)
{
    //si la valeur a ajouter est positive
    if(deltaHP >= 0)
    {
        //selon la quantité à ajouter on met à vie max ou on ajoute simplement deltahp
        if(_hp + deltaHP > 100)
            _hp = START_HP;
        else
            _hp += deltaHP;

        //on retourne faux : pas de mise à jour du nombre de vies
        return false;
    }
    else
    {
        //sinon on retire la vie
        if(_hp + deltaHP <= 0)
        {
            _hp = 0;
            //on retourne faux : pas de mise à jour du nombre de vies
            return true;
        }
        else
        {
            _hp += deltaHP;
            //on retourne vrai : mise à jour du nombre de vies
            return false;
        }
    }
}

bool Ship::canShoot() const
{

    return(_clockShoot.GetElapsedTime() * 1000 - _lastShoot >= _fireSpeed);
}

Bullet* Ship::shoot(int bullet_speed, int damage)
{
    Bullet * b = new Bullet(this, bullet_speed, damage);
    _lastShoot = _clockShoot.GetElapsedTime() * 1000;
    return b;
}

//Getters

int Ship::getHP() const
{
    return _hp;
}

unsigned int Ship::getMaxHealth() const
{
    return START_HP;
}
