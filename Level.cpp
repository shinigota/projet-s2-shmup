#include "Level.hpp"
#include "Enemy.hpp"
#include "Player.hpp"
#include "GameView.hpp"
#include <iostream>

using namespace std;

Level::Level(int seed, unsigned int difficulty)
    :_seed(difficulty), _difficulty(5), _totalEnemies(seed % 20 + 30), _deltaTime(seed % 2 + 2), _startClock(clock()), _lastEnemySpawn(0), _enemies(set<Enemy*>())
{}

Level::~Level()
{
    for(auto enemyIt : _enemies)
        delete enemyIt;

    _enemies.clear();
}


bool Level::isFinished() const
 {
     return (_enemies.size() == 0 && _totalEnemies == 0 ) ;
 }


bool Level::canGenerateEnemy() const
{
    return ((time(NULL)- _lastEnemySpawn)  >= _deltaTime) && _totalEnemies > 0;
}

void Level::generateEnemy(int xShip)
{
    int nb = rand();
    int y = nb % (GameView::DEF_SCREEN_H);
    int type = nb % 3;

    Enemy * e;
    if (_difficulty % 3 == 0 && _totalEnemies == 1)
        e = Enemy::Boss(xShip + GameView::DEF_SCREEN_W + 100, y, _difficulty);
    else
        e = new Enemy(xShip + GameView::DEF_SCREEN_W + 100, y, Enemy::DEF_DX, Enemy::DEF_DY, _difficulty, type);


    _enemies.insert(e);
    _lastEnemySpawn = time(NULL);
    _totalEnemies --;
}

set<Enemy*> * Level::getEnemies()
{
    return &_enemies;
}

string Level::toString() const
{
    return "Level : seed : " + to_string(_seed) + " - difficulty : " + to_string(_difficulty) + " - total ennemies : " + to_string(_totalEnemies) + " - time before spawn : " + to_string(_deltaTime);
}

int Level::getNumberOfEnemies() const
{
    return _totalEnemies;
}

