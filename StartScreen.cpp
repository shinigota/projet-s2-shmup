#include "StartScreen.hpp"
#include "Game.hpp"
#include "MenuScreen.hpp"

using namespace std;
#include <iostream>

StartScreen::StartScreen(sf::RenderWindow * window, Game * game)
    : _game(game), _window(window), _moveClock(), _firstMoveDone(false)
{
    _textFont.LoadFromFile("fonts/forcedsquare.ttf");
    _title = sf::String("Shoot Them Up", _textFont, 100);
    _title.SetPosition(- _title.GetRect().GetWidth(), _window->GetHeight() / 2 - _title.GetRect().GetHeight());
}

StartScreen::~StartScreen()
{}

void StartScreen::draw()
{
    _window->Clear(sf::Color::Black);
    _window->Draw(_title);
}

void StartScreen::update(float fps)
{
    //mise à jour du mouvement du titre
    if(!_firstMoveDone && _title.GetPosition().x <=  _window->GetWidth() / 2 - _title.GetRect().GetWidth() / 2)
        _title.Move(400 * 1/fps, 0);
    else if (!_firstMoveDone)
    {
        _firstMoveDone = true;
        _title.SetPosition(_window->GetWidth() / 2 - _title.GetRect().GetWidth() / 2, _window->GetHeight() / 2 - _title.GetRect().GetHeight());
    }


    if(_firstMoveDone && _title.GetPosition().y >= 20)
        _title.Move(0, -400 * 1/fps);
    else if (_firstMoveDone)
        _game->setScreen(MenuScreen::menuMain(_window, _game));
}
