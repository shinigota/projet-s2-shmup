#ifndef BULLET_HPP
#define BULLET_HPP

#include "MovableEntity.hpp"

class Ship;

class Bullet : public MovableEntity {
private:
    unsigned int _dmg;
    int _enemyType;
    Ship * _s;
public:
    static const unsigned int  DEF_BULLET_WIDTH= 20, DEF_BULLET_HEIGHT = 4, DEF_BULLET_DMG= 15;
    static const int  DEF_BULLET_SPEED = 700;

    Bullet(Ship * s,int x, int y, int dx, unsigned int dmg);
    Bullet(Ship * s, int dx, unsigned int dmg);
    virtual ~Bullet();


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  string containing bullet'se
    // Purpose: display text format of a bullet
    //------------------------------------------------------------------------------
    std::string toString() const override;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  bool about bullet's owner is an enemy or no
    // Purpose: knowing if a bullet comes from player or enemy (used in collisions)
    //------------------------------------------------------------------------------
    bool isEnemy() const;


    //Getters

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  amount of damage of the bullet
    // Purpose: knowing the damage the current bullet will deal
    //------------------------------------------------------------------------------
    unsigned int getDamage() const;


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  Ship *, bullet's owner
    // Purpose: used for ennemies in order to draw the bullet in a different way depending on their level
    //------------------------------------------------------------------------------
    Ship * const getShip() const;


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  Enemy's type (if owner is an enemy)
    // Purpose: used for ennemies in order to draw the bullet in a different way depending on their level. Useless if owner is a player
    //------------------------------------------------------------------------------
    int getEnemyType() const;
};
#endif // BULLET_HPP


