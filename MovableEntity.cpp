#include "MovableEntity.hpp"


using namespace std;

MovableEntity::MovableEntity(float x, float y, unsigned int w, unsigned int h, float dx, float dy)
    :Entity(x, y, w, h), _dx(dx), _dy(dy) {}

MovableEntity::MovableEntity(float x, float y, float dx, float dy)
    :Entity(x, y), _dx(dx), _dy(dy) {}

MovableEntity::MovableEntity()
    :Entity(), _dx(0), _dy(0) {}

MovableEntity::~MovableEntity() {}

void MovableEntity::updateX(float elapsedTime)
{
    _x += _dx * elapsedTime;
}

void MovableEntity::updateY(float elapsedTime)
{
    _y += _dy * elapsedTime;
}

string MovableEntity::toString() const
{
    return "MovableEntity : " + Entity::toString() + " - x_speed : " + to_string(_dx) + " - y_speed : " + to_string(_dy);
}


//Getters

float MovableEntity::getDX() const
{
    return _dx;
}

float MovableEntity::getDY() const
{
    return _dy;
}


//Setters

void MovableEntity::setDX(float dx)
{
    _dx = dx;
}

void MovableEntity::setDY(float dy)
{
    _dy = dy;
}
