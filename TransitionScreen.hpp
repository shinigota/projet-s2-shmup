#ifndef TRANSITIONSCREEN_HPP
#define TRANSITIONSCREEN_HPP

#include "Screen.hpp"
#include <SFML/Graphics.hpp>

class Game;

class TransitionScreen : public Screen {
private:
    Game * _game;
    sf::Font font;
    sf::String text;
    int _w, _h;
    long screenStart;
    sf::RenderWindow * _window;
    const static unsigned int TRANSITION_TIME_SEC = 2;
    std::function<void(sf::RenderWindow *, Game *)> _nextScreenAction;
    TransitionScreen(sf::RenderWindow * window, Game * game, std::string str, std::function<void(sf::RenderWindow *, Game *)> nextScreenAction);
public:

    static TransitionScreen * TransitionNextLevel(sf::RenderWindow * window, Game * game, int level);
    static TransitionScreen * TransitionGameOver(sf::RenderWindow * window, Game * game);

    ~TransitionScreen();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: drawing the current screen
    //------------------------------------------------------------------------------
    void draw() override;

    //------------------------------------------------------------------------------
    // Input: fps, the number of frame per second the screen will use to update (same update speed with fps)
    // Output:  /
    // Return:  /
    // Purpose: updating the current screen
    //------------------------------------------------------------------------------
    void update(float fps) override;
};
#endif
