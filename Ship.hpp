#ifndef SHIP_HPP
#define SHIP_HPP

#include "MovableEntity.hpp"
#include <set>
#include <SFML/Graphics.hpp>

class Bullet;

class Ship : public MovableEntity {
protected:
    int _hp;
    sf::Clock _clockShoot;
    double _fireSpeed, _lastShoot, _damage;

public:
    static const int START_HP = 100, DEF_DX = 100, DEF_DY = 300, DEF_FIRE_SPEED = 750;
    Ship(float x, float y, unsigned int w, unsigned int h, float dx, float dy, int hp = START_HP);
    Ship(float x, float y, float dx, float dy, float hp = START_HP);
    Ship();
    ~Ship() override;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  string containing ship's informations
    // Purpose: display text version of a ship
    //------------------------------------------------------------------------------
    std::string toString() const override;

    //------------------------------------------------------------------------------
    // Input: deltaHP : amount of HP (positive or negative) to add to the player
    // Output:  /
    // Return:  true if ship's life reaches 0 (no more life, dead) , otherwise false
    // Purpose: updating an ship's health and life points and knowing if he's dead
    //------------------------------------------------------------------------------
    virtual bool updateHp(int deltaHP);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  true if the ship can shoot, false otherwise
    // Purpose: knowing, according to the date of the last shot and the fire rate if the ship can shoot
    //------------------------------------------------------------------------------
    virtual bool canShoot() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  a new bullet with bullet_speed and damage
    // Purpose: generating a new bullet with the correct x speed and coordinates
    //------------------------------------------------------------------------------
    Bullet * shoot(int bullet_speed, int damage);

    //Getters
    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  number of health points left (between 1 and 100)
    // Purpose: knowing ship's hp
    //------------------------------------------------------------------------------
    int getHP() const;

        //Getters
    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  number of lives left (between 0 and 3)
    // Purpose: knowing ship's lives left
    //------------------------------------------------------------------------------
    int getLife() const;


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return: the max health of the ship
    // Purpose: knowing the max health of the current ship
    //------------------------------------------------------------------------------
    virtual unsigned int getMaxHealth() const;

};
#endif // SHIP_HPP


