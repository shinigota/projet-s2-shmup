#ifndef GRAPHICELEMENT_HPP
#define GRAPHICELEMENT_HPP

#include <SFML/Graphics.hpp>

class GraphicElement : public sf::Sprite {
private:
    int _w, _h;
    float _x, _y;
    int getX();
public:
    static const int DEF_W = 15, DEF_H = 15, DEF_X = 0, DEF_Y = 0;
    GraphicElement();
    GraphicElement(sf::Image * image, int x, int y, int w, int h, float rotation);

    virtual ~GraphicElement();

    //------------------------------------------------------------------------------
    // Input: xOrigin a gap for drawing, RenderWindow * windo the window to draw into
    // Output:  /
    // Return:  /
    // Purpose: drawing the game with a xOrigin gap on x-axis in a particular window
    //------------------------------------------------------------------------------
    virtual void draw(float xOrigin, sf::RenderWindow * window);

    //------------------------------------------------------------------------------
    // Input: int x, int y the new position
    // Output:  /
    // Return:  /
    // Purpose: changing the current element's position
    //------------------------------------------------------------------------------
    void setPosition(int x, int y);

    //------------------------------------------------------------------------------
    // Input: Vector2f pos the new position
    // Output:  /
    // Return:  /
    // Purpose: changing the current element's position
    //------------------------------------------------------------------------------
    void setPosition(const sf::Vector2f & pos);

    //------------------------------------------------------------------------------
    // Input: int w, int h the new size
    // Output:  /
    // Return:  /
    // Purpose: changing the current element's size
    //------------------------------------------------------------------------------
    void resize(int w, int h);

};

#endif // GRAPHICELEMENT_HPP
