#include "Enemy.hpp"
#include "Bullet.hpp"
#include "GameView.hpp"
#include "Item.hpp"
#include <iostream>

using namespace std;

Enemy::Enemy(float x, float y, float dx, float dy, unsigned int lvl, int type)
    :Ship( x, y, dx, dy, 90 + lvl * 10), _lvl(lvl), _drop(generateDropNumber()), _pointsOnKill(5 + lvl * 5), _type(type)
{
    _damage = _lvl * 10 + type * 3;
    _fireSpeed = Ship::DEF_FIRE_SPEED + type * 100;
    _dx = _dx - 50 * type;
}

Enemy::Enemy(float x, float y, unsigned int w, unsigned int h, float dx, float dy, unsigned int lvl, int type)
    :Ship( x, y, w, h, dx, dy, 90 + lvl * 10), _lvl(lvl), _drop(generateDropNumber()), _pointsOnKill(5 + lvl * 5), _type(type)
{
    _damage = _lvl * 10 + type * 3;
    _fireSpeed = Ship::DEF_FIRE_SPEED + type * 100;
    _dx = _dx - 50 * type;
}

Enemy * Enemy::Boss(float x, float y, unsigned int lvl)
{
    Enemy * e = new Enemy(x, y, Enemy::DEF_BOSS_W, Enemy::DEF_BOSS_H, Enemy::DEF_DX, Enemy::DEF_DY, lvl + 10, 0);
    return e;
}

Enemy::~Enemy()
{}

void Enemy::moveToPlayerShip(const Ship & playerShip, float elapsedTime)
{
    if(playerShip.getX() + (int) GameView::DEF_SCREEN_W - 100 >= _x)
    {
        if(playerShip.getY() + playerShip.getH() / 2 > _y  + _h / 2 + DEF_DY * elapsedTime)
        {
            _dy = DEF_DY;
        }
        else if(playerShip.getY() + playerShip.getH() / 2 < _y + _h / 2 - DEF_DY * elapsedTime)
        {
            _dy = -DEF_DY;
        }
        else
        {
            _y = playerShip.getY() + playerShip.getH() / 2 -_h / 2;
            _dy = 0;
        }
    }
    else
    {
        _dy = 0;
    }
}

bool Enemy::canShoot(const Ship & playerShip) const
{
    return Ship::canShoot() && distance(playerShip) <= (int) GameView::DEF_SCREEN_W - 100;
}

Bullet* Enemy::shoot()
{
  return Ship::shoot(-Bullet::DEF_BULLET_SPEED, _damage);
}


string Enemy::toString() const {
    return "Enemy : " + Ship::toString() + " - level : " + to_string(_lvl);
}

bool Enemy::updateHp(int deltaHP)
{
    if(deltaHP + _hp <= 0)
    {
        _hp = 0;
        return true;
    }

    _hp += deltaHP;
    return false;
}

Item * Enemy::dropItem() const
{
    if(_drop >= 0 && _drop < 20)
    {
        return Item::HealthPackBonus(_x + _w/2, _y + _h/2);
    }
    else if (_drop >= 20 && _drop < 40)
    {
        return Item::DoubleBulletsBonus(_x + _w/2, _y + _h/2);
    }
    else if (_drop >= 40 && _drop < 60)
    {
        return Item::DoubleFireRateBonus(_x + _w/2, _y + _h/2);
    }
    else if (_drop >= 60 && _drop < 80)
    {
        return Item::CoinBonus(_x + _w/2, _y + _h/2);
    }
    else if (_drop >= 80 && _drop < 90)
    {
        return Item::BombBonus(_x + _w/2, _y + _h/2);
    }

    return nullptr;
}

int Enemy::generateDropNumber()
{
    return rand()%101;
}

unsigned int Enemy::getPointsOnKill() const
{
    return _pointsOnKill;
}

unsigned int Enemy::getMaxHealth() const
{
    return 90 + _lvl * 10;
}

int Enemy::getType() const
{
    return _type;
}

