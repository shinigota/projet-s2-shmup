#ifndef SHMUP_H
#define SHMUP_H


#include <SFML/Graphics.hpp>
#include "Screen.hpp"
#include "GameModel.hpp"
#include "GameView.hpp"
#include "Game.hpp"

class GameModel;
class GameView;

class GameScreen : public Screen {
private:
    sf::RenderWindow * _window;
    GameModel _model;
    GameView _view;
    Game * _game;
    std::function<void(Game *)> _nextScreenAction;

public:
    GameScreen(sf::RenderWindow * window, Game * game, bool defaultGraphics);
    virtual ~GameScreen();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: drawing the GameModel _model
    //------------------------------------------------------------------------------
    void draw() override;

    //------------------------------------------------------------------------------
    // Input: fps, the number of frame per second the current screen will use to update (same update speed with fps)
    // Output:  /
    // Return:  /
    // Purpose: updating the current GameScreen (nextStep and synchronize)
    //------------------------------------------------------------------------------
    void update(float fps) override;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: call _view->initSprites() to reload sprites
    //------------------------------------------------------------------------------
    void reloadGraphics();

    //Getter
    Player * getPlayer();

};

#endif // SHMU_H

