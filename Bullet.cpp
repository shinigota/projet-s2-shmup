#include "Bullet.hpp"
#include "GameView.hpp"
#include "Ship.hpp"
#include "Enemy.hpp"

using namespace std;

Bullet::Bullet(Ship* s, int x, int y, int dx, unsigned int dmg)
    :MovableEntity(x, y, DEF_BULLET_WIDTH, DEF_BULLET_HEIGHT, dx, 0), _dmg(dmg), _enemyType(-1), _s(s)
{
    if(dynamic_cast<Enemy *>(s) != nullptr)
    {
        Enemy * e = (Enemy *) s;
        _enemyType = e->getType();
    }

}

Bullet::Bullet(Ship * s, int dx, unsigned int dmg)
    :MovableEntity(s->getX() - s->getW()/2, s->getY() + s->getH()/2, DEF_BULLET_WIDTH, DEF_BULLET_HEIGHT, dx, 0), _dmg(dmg), _enemyType(-1), _s(s)
{
    if(dynamic_cast<Enemy *>(s) != nullptr)
    {
        Enemy * e = (Enemy *) s;
        _enemyType = e->getType();
    }

}

Bullet::~Bullet()
{}

string Bullet::toString() const
{
    //Si direction = à gauche alors ennemi, sinon joueur
    string bulletOwner = _dx<0?"Enemy":"Player";
    return bulletOwner + " bullet : " + MovableEntity::toString() + " damage : " + to_string(_dmg);
}

bool Bullet::isEnemy() const
{
    return _dx <= 0;
}

//Getters

 unsigned int Bullet::getDamage() const
{
    return _dmg;
}

Ship * const Bullet::getShip() const
{
    return _s;
}

int Bullet::getEnemyType() const
{
    return _enemyType;
}
