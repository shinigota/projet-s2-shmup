#ifndef _SHOPSCREEN_H_
#define _SHOPSCREEN_H_

#include "MenuScreen.hpp"
#include <SFML/Graphics.hpp>

class ShopScreen : public MenuScreen
{
public:
    ShopScreen(sf::RenderWindow * window, Game * game);
    virtual ~ShopScreen();
    virtual void draw();
    virtual void update(float fps);

private:
    std::set<sf::String *> _text;
};

#endif // _SHOPSCREEN_H_
