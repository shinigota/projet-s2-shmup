#include "TransitionScreen.hpp"
#include "Game.hpp"
#include "MenuScreen.hpp"
#include "ShopScreen.hpp"

using namespace sf;
using namespace std;

TransitionScreen::TransitionScreen(sf::RenderWindow * window, Game * game, string str, function<void(RenderWindow *, Game *)> nextScreenAction)
    :_game(game), _w(GameView::DEF_SCREEN_W), _h(GameView::DEF_SCREEN_H), screenStart(time(NULL)), _window(window), _nextScreenAction(nextScreenAction)
{
    font.LoadFromFile("fonts/forcedsquare.ttf");
    text = String(str, font, 50);
    text.Move(_w/2 - text.GetRect().GetWidth() / 2 ,  _h/2 - text.GetRect().GetHeight() / 2);
}

TransitionScreen *
TransitionScreen::TransitionNextLevel( sf::RenderWindow * window, Game * game, int level)
{
    //après transition : magasin
    function<void(RenderWindow *, Game *)> nextScreenAction = [](RenderWindow * window, Game * g)
    {
       g->setScreen(new ShopScreen(window, g));
    };

    string m = "Level " + to_string(level) +" complete";
    TransitionScreen * t = new TransitionScreen(window, game, m, nextScreenAction);
    return t;
}


TransitionScreen *
TransitionScreen::TransitionGameOver( sf::RenderWindow * window, Game * game)
{
    //après transition : menu principal
    function<void(RenderWindow *, Game *)> nextScreenAction = [](RenderWindow * window, Game * g)
    {
       g->setScreen(MenuScreen::menuMain(window, g));
    };
    string m = "Game Over";
    TransitionScreen * t = new TransitionScreen(window, game, m, nextScreenAction);
    return t;
}


TransitionScreen::~TransitionScreen()
{}

void TransitionScreen::draw()
{
    _window->Clear(sf::Color::Black);
    _window->Draw(text);
}

void TransitionScreen::update(float fps)
{
    if(time(NULL) - screenStart > TRANSITION_TIME_SEC)
        _nextScreenAction(_window, _game);
}
