#ifndef ITEM_HPP
#define ITEM_HPP

#include "Entity.hpp"
#include <functional>

class Player;

enum ItemType { Coin, HealthPack, FireRate, DoubleBullets, Bomb };

class Item : public Entity
{
private:
    Item(int x, int y, ItemType type, std::function<void(Player *)> action);
    ItemType _itemType;
    std::function<void(Player *)> _action;

public:
    const static int HP_BONUS = 30;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  an item pointer
    // Purpose: create a double fire rate bonus at the x and y coordinates
    //------------------------------------------------------------------------------
    static Item * DoubleFireRateBonus(int x, int y);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  an item pointer
    // Purpose: create a double bullets bonus at the x and y coordinates
    //------------------------------------------------------------------------------
    static Item * DoubleBulletsBonus(int x, int y);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  an item pointer
    // Purpose: create a health bonus at the x and y coordinates
    //------------------------------------------------------------------------------
    static Item * HealthPackBonus(int x, int y);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  an item pointer
    // Purpose: create a coin bonus at the x and y coordinates
    //------------------------------------------------------------------------------
    static Item * CoinBonus(int x, int y);


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  an item pointer
    // Purpose: create a bomb bonus at the x and y coordinates
    //------------------------------------------------------------------------------
    static Item * BombBonus(int x, int y);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  string containing item's informations
    // Purpose: display text version of an item
    //------------------------------------------------------------------------------
    std::string toString() const override;


    //------------------------------------------------------------------------------
    // Input: p, the player the item has to act on
    // Output:  /
    // Return:  /
    // Purpose: activating the bonus on the player
    //------------------------------------------------------------------------------
    void act(Player * p) const;

    //Getters

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  the type of the current item
    // Purpose: knowing and item's type
    //------------------------------------------------------------------------------
    ItemType getType() const;
};

#endif // ITEM_HPP

