#include "Entity.hpp"
#include "GameView.hpp"
#include <iostream>
#include <cmath>
using namespace std;

Entity:: Entity(float x, float y, unsigned int w, unsigned int h)
    :_x(x), _y(y), _w(w), _h(h) {}

Entity::Entity(float x, float y)
    :_x(x), _y(y), _w(DEF_W), _h(DEF_H) {}

Entity::Entity()
    :_x(GameView::DEF_SCREEN_W/2 - DEF_W/2), _y(GameView::DEF_SCREEN_H/2 - DEF_H/2), _w(DEF_W), _h(DEF_H) {}

Entity::~Entity(){}

bool Entity::collides(const Entity & e) const
{
    return ! ( e._y + (float) e._h < _y || e._x + (float) e._w < _x || e._y > _y +(float) _h ||_x + (float) _w < e._x );
}


bool Entity::outOfScreen(float xOrigin, unsigned int screenWidth, unsigned int screenHeight) const
{
    return (_x + (float) _w < xOrigin - 10 || _y > (float) screenHeight || _y + (float) _h < 0 || _x >= xOrigin + 2 * GameView::DEF_SCREEN_W);
}

string Entity::toString() const
{
    return "Entity : x : " + to_string(_x) + " - y : " + to_string(_y) + " - width : " + to_string(_w) + " - height : " + to_string(_h);
}

float Entity::distance(const Entity & e) const
{
    float e1OriginX =  _x + _w/2, e1OriginY = _y + _h/2;
    float e2OriginX =  e._x + e._w/2, e2OriginY = e._y + e._h/2;

    return (float) sqrt((e2OriginX - e1OriginX)*(e2OriginX - e1OriginX) + (e2OriginY - e1OriginY)*(e2OriginY - e1OriginY));
}

//Getters
float Entity::getX() const
{
    return _x;
}

float Entity::getY() const
{
    return _y;
}

unsigned int Entity::getW() const
{
    return _w;
}

unsigned int Entity::getH() const
{
    return _h;
}

//Setters
void Entity::setX(float x)
{
    _x = x;
}

void Entity::setY(float y)
{
    _y = y;
}
