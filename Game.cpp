#include "Game.hpp"
#include "MenuScreen.hpp"
#include "GameScreen.hpp"
#include "TransitionScreen.hpp"
#include "StartScreen.hpp"

#include <iostream>
#include "Player.hpp"

using namespace std;
using namespace sf;

Game::Game(RenderWindow * window)
    : _window(window), _screen(new StartScreen(_window, this)), _gameScreen(nullptr), _defaultGraphics(true), _play(true)
{}

Game::~Game()
{
    delete _screen;

    if(_gameScreen != nullptr)
        delete _gameScreen;

    delete _window;
}

void Game::update(float fps)
{
    _screen->update(fps);
}

void Game::draw()
{
    _screen->draw();
}

void Game::setScreen(Screen * screen)
{
    if(dynamic_cast<GameScreen *>(_screen) == nullptr)
        delete _screen;
    _screen = screen;
}

void Game::display()
{
    _window->Display();
}

void Game::backToGameScreen()
{
    delete _screen;
    _screen = _gameScreen;
}

void Game::transitionScreenNextLevel(int level)
{
    setScreen(TransitionScreen::TransitionNextLevel(_window, this, level));
}

void Game::transitionScreenGameOver()
{
    setScreen(TransitionScreen::TransitionGameOver(_window, this));

}

RenderWindow * Game::getWindow()
{
    return _window;
}

bool Game::defaultGraphics()
{
    return _defaultGraphics;
}

Player * Game::getPlayer()
{
    return _gameScreen->getPlayer();
}

void Game::setDefaultGraphics(bool val)
{
    _defaultGraphics = val;
}

void Game::startGame()
{
    _gameScreen = new GameScreen(_window, this, _defaultGraphics);
    _screen = _gameScreen;
}

void Game::stopGame()
{
    _play = false;
}

bool Game::continuePlaying()
{
    return _play;
}
