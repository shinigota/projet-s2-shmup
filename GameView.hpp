#ifndef GAMEVIEW_HPP
#define GAMEVIEW_HPP

#include "Screen.hpp"
#include <set>
#include <map>
#include <SFML/Graphics.hpp>


class GameModel;
class Game;
class Enemy;
class Bullet;
class Item;
class Level;
class Player;
class GraphicElement;
class MovableEntity;
class Ship;

class GameView
{
private:
    static const int UI_IMAGE_SIZE = 32;
    Game * _game;
    GameModel * _model;
    unsigned int _w, _h;
    std::set<Enemy *> * _enemies;
    std::set<Bullet *> * _bullets;
    std::set<Item *> * _items;
    Level * _level;
    Player * _player;
    std::map<MovableEntity *, GraphicElement *> _elementToGraphicElement;

    std::map<Item *, GraphicElement *> _itemToGraphicElement;

    sf::RenderWindow * _window;

    sf::Font scoreFont;
    sf::String scoreText;
    sf::String cashText;

    sf::Image _background_image;
    sf::Sprite _background_sprite;
    sf::Image _background_image2;
    sf::Sprite _background_sprite2;

    sf::Image _ship_player_image;
    sf::Image _ship_1_image;
    sf::Image _ship_2_image;
    sf::Image _ship_3_image;

    sf::Image _bullet_1_image;
    sf::Image _bullet_2_image;
    sf::Image _bullet_3_image;

    sf::Image _hpack_image;
    sf::Image _doublefire_image;
    sf::Image _firerate_image;
    sf::Image _coin_image;
    sf::Image _bomb_bonus_image;

    sf::Image _heart_image;
    sf::Sprite _heart_sprite;
    sf::Image _bomb_image;
    sf::Sprite _bomb_sprite;

    sf::Image _thrust_slow_image;
    sf::Sprite _thrust_slow_sprite;

    sf::Image _thrust_normal_image;
    sf::Sprite _thrust_normal_sprite;

    sf::Image _thrust_fast_image;
    sf::Sprite _thrust_fast_sprite;

    bool usedBomb;
public:
    static const unsigned int DEF_SCREEN_H=600, DEF_SCREEN_W=800, X_ORIGIN = 50;
    static const std::string VARIANT_BG;
    static const std::string DEFAULT_BG;
    static const std::string VARIANT_SHIP_PLAYER;
    static const std::string VARIANT_SHIP1;
    static const std::string VARIANT_SHIP2;
    static const std::string VARIANT_SHIP3;
    static const std::string DEFAULT_SHIP_PLAYER;
    static const std::string DEFAULT_SHIP1;
    static const std::string DEFAULT_SHIP2;
    static const std::string DEFAULT_SHIP3;
    static const std::string VARIANT_BULLET1;
    static const std::string VARIANT_BULLET2;
    static const std::string VARIANT_BULLET3;
    static const std::string DEFAULT_BULLET1;
    static const std::string DEFAULT_BULLET2;
    static const std::string DEFAULT_BULLET3;
    static const std::string VARIANT_HPACK;
    static const std::string VARIANT_COIN;
    static const std::string VARIANT_DOUBLEFIRE;
    static const std::string VARIANT_FIRERATE;
    static const std::string VARIANT_BOMBBONUS;
    static const std::string VARIANT_BOMB;
    static const std::string VARIANT_HEART;
    static const std::string DEFAULT_HEART;
    static const std::string VARIANT_THRUSTSLOW;
    static const std::string VARIANT_THRUSTNORMAL;
    static const std::string VARIANT_THRUSTFAST;



    GameView(Game * game, bool defaultGraphics, GameModel * model);
    ~GameView();

    //a modif
    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: loading all the images and sprites
    //------------------------------------------------------------------------------
    void initSprites(bool defaultGraphics);

    //------------------------------------------------------------------------------
    // Input: model, the model to link with the view
    // Output:  /
    // Return:  /
    // Purpose: linking model and view
    //------------------------------------------------------------------------------
    void setModel(GameModel * model);


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: rendering entities in text format
    //------------------------------------------------------------------------------
    void draw();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  false if quit event, else true
    // Purpose: treating events and knowing if view still has to treat them
    //------------------------------------------------------------------------------
    bool treatEvents();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: treating events and knowing if view still has to treat them
    //------------------------------------------------------------------------------
    void synchronize();


    //------------------------------------------------------------------------------
    // Input: Ship ship, the ship the health bar will be drawn under
    // Output:  /
    // Return:  /
    // Purpose: draw a ship's health bar
    //------------------------------------------------------------------------------
    void drawHealth(const Ship & ship);


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: drawing user's interface (cash, score, health, lives and bombs)
    //------------------------------------------------------------------------------
    void drawInterface();

};

#endif // GAMEVIEW_HPP

