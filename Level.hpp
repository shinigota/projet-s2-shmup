#include <set>
#include <time.h>
#include <string>

class Enemy;

class Level{

private:
    int _seed, _difficulty, _totalEnemies, _deltaTime;
    clock_t _startClock;
    double _lastEnemySpawn;
    std::set<Enemy*> _enemies;

public:
    Level(int seed, unsigned int difficulty);
    ~Level();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  yes if there's no more enemies otherwise no
    // Purpose: knowing if all the enemies are dead and there's no more to generate
    //------------------------------------------------------------------------------
    bool isFinished() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  yes if enough time has happened since last spawn, else no
    // Purpose: knowing if the level can generate an enemy depending on the delay of spawn
    //------------------------------------------------------------------------------
    bool canGenerateEnemy() const;

    //------------------------------------------------------------------------------
    // Input: xShip player's x position to generate the enemy with a certain distance
    // Output:  /
    // Return:  /
    // Purpose: generating a new enemy
    //------------------------------------------------------------------------------
    void generateEnemy(int xShip);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  string containing level's informations
    // Purpose: display text version of a level
    //------------------------------------------------------------------------------
    std::string toString() const;

    //Getters
    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  pointer of set of enemies
    // Purpose: having a pointer of the set of enemies of the current level
    //------------------------------------------------------------------------------
    std::set<Enemy*> * getEnemies();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  the number of enemies left
    // Purpose: knowing the numbers of enemies left + numbers of enemies that need to spawn
    //------------------------------------------------------------------------------
    int getNumberOfEnemies() const;


};
