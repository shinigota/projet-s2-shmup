#ifndef ENEMY_HPP
#define ENEMY_HPP

#include "Ship.hpp"

class GameModel;
class Item;

class Enemy : public Ship {
private:
    unsigned int _lvl, _drop, _pointsOnKill;
    int _type;
public:
    static const int DEF_LEVEL = 1, DEF_DX = -100, DEF_DY = 100, DEF_BOSS_W = 70, DEF_BOSS_H = 70;

    Enemy(float x, float y, float dx, float dy, unsigned int lvl, int type);

    Enemy(float x, float y, unsigned int w, unsigned int  h, float dx, float dy, unsigned int lvl, int type);

    //------------------------------------------------------------------------------
    // Input: x and y  :coordinates of the boss, lvl : the difficulty of the boss
    // Output:  /
    // Return:  a new enemy
    // Purpose: making stronger and larger enemies
    //------------------------------------------------------------------------------
    static Enemy * Boss(float x, float y, unsigned int lvl);

    ~Enemy() override;

    //------------------------------------------------------------------------------
    // Input: playerShip : player ship enemy has to follow, elapsedTime the time since the last update
    // Output:  /
    // Return:  /
    // Purpose: changing Y speed depending on player's position
    //------------------------------------------------------------------------------
    void moveToPlayerShip(const Ship & playerShip, float elapsedTime);


    //------------------------------------------------------------------------------
    // Input: the player's ship
    // Output:  /
    // Return:  true if the enemy can shoot, otherwise false
    // Purpose: knowing, according the the rate of fire and player's distance if the enemy can shoot
    //------------------------------------------------------------------------------
    bool canShoot(const Ship & playerShip) const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  new bullet
    // Purpose: creating a new bullet with the correct speed and current enemy's position
    //------------------------------------------------------------------------------
    Bullet* shoot();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  string containing enemy's informations
    // Purpose: display text version of an enemy
    //------------------------------------------------------------------------------
    std::string toString() const override;

    //------------------------------------------------------------------------------
    // Input: deltaHP : amount of HP (positive or negative) to add to the enemy
    // Output:  /
    // Return:  true if enemy's health reaches , otherwise false
    // Purpose: updating an enemy's health and knowing if he's dead
    //------------------------------------------------------------------------------
    bool updateHp(int deltaHP) override;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  a new item
    // Purpose: generating a item or a null pointer according to drop number of the current enemy
    //------------------------------------------------------------------------------
    Item * dropItem() const;


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  an int corresponding to the item to drop
    // Purpose: dropping or not an item on the death of the enemy
    //------------------------------------------------------------------------------
    static int generateDropNumber();

    //Getters
    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  int : quantity of points the enemy gives
    // Purpose: increasing player's score when the enemy is killed
    //------------------------------------------------------------------------------
    unsigned int getPointsOnKill() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  int : the max health
    // Purpose: knowing the max health of the ennemy
    //------------------------------------------------------------------------------
    virtual unsigned int getMaxHealth() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  int : the type of the ennemy
    // Purpose: knowing the type of the ennemy
    //------------------------------------------------------------------------------
    int getType() const;


};

#endif // ENEMY_HPP
