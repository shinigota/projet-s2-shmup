#define BOOST_TEST_MODULE TestsShmup
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <ctime>
#include <set>

#include "Player.hpp"
#include "Bullet.hpp"
#include "Enemy.hpp"
#include "Item.hpp"
#include "Level.hpp"

#include <iostream>
#include <string>

using namespace std;

static float elapsedTime = 1;

BOOST_AUTO_TEST_CASE( test_Entity )
{
    Entity * e1 = new Entity(0, 1, 5, 5);
    BOOST_CHECK(e1->getX() == 0);
    BOOST_CHECK(e1->getY() == 1);
    BOOST_CHECK(e1->getW() == 5);
    BOOST_CHECK(e1->getH() == 5);

    Entity * e2 = new Entity(-1, -1);
    BOOST_CHECK(e2 ->getX() == -1);
    BOOST_CHECK(e2 ->getY() == -1);
    BOOST_CHECK(e2 ->getW() == 30);
    BOOST_CHECK(e2 ->getH() == 30);

    Entity * e3 = new Entity(6, 1, 5, 5);

    Entity * e4 = new Entity(-6, 1, 5, 5);

    Entity * e5 = new Entity(0, -5, 5, 5);

    Entity * e6 = new Entity(0, 100, 5, 5);

    Entity * e7 = new Entity(0, 800, 5, 5);
    Entity * e8 = new Entity(0, -800, 5, 5);

    BOOST_CHECK(e1->collides(*e2));
    BOOST_CHECK(! e1->collides(*e3));
    BOOST_CHECK(! e1->collides(*e4));
    BOOST_CHECK(! e1->collides(*e5));
    BOOST_CHECK(! e1->collides(*e6));

    BOOST_CHECK(!e1->outOfScreen(e1->getX(), 50, 50));
    BOOST_CHECK(e6->outOfScreen(e1->getX(), 50, 50));
    BOOST_CHECK(e7->outOfScreen(e1->getX(), 50, 50));
    BOOST_CHECK(e8->outOfScreen(e1->getX(), 50, 50));
    //Rajouter pour la gauche et le haut ?

    BOOST_CHECK(e1->distance(*e3) == 6);
    BOOST_CHECK(e1->distance(*e4) == 6);
    BOOST_CHECK(e1->distance(*e5) == 6);
    BOOST_CHECK(e1->distance(*e6) == 99);

    e1->setX(-87);
    BOOST_CHECK(e1->getX() == -87);
    e1->setX(213);
    BOOST_CHECK(e1->getX() == 213);
    e1->setX(0);
    BOOST_CHECK(e1->getX() == 0);

    e1->setY(-87);
    BOOST_CHECK(e1->getY() == -87);
    e1->setY(213);
    BOOST_CHECK(e1->getY() == 213);
    e1->setY(0);
    BOOST_CHECK(e1->getY() == 0);

    delete e1;
    delete e2;
    delete e3;
    delete e4;
    delete e5;
    delete e6;
    delete e7;
    delete e8;
}

BOOST_AUTO_TEST_CASE( test_MovableEntity )
{
    MovableEntity * m1 = new MovableEntity(0, 0, 20, 20, 10, 0);
    BOOST_CHECK(m1->getX() == 0);
    BOOST_CHECK(m1->getY() == 0);
    BOOST_CHECK(m1->getW() == 20);
    BOOST_CHECK(m1->getH() == 20);
    BOOST_CHECK(m1->getDX() == 10);
    BOOST_CHECK(m1->getDY() == 0);

    MovableEntity * m2 = new MovableEntity(-50, 50, -3, 3);
    BOOST_CHECK(m2->getX() == -50);
    BOOST_CHECK(m2->getY() == 50);
    BOOST_CHECK(m2->getW() == 30);
    BOOST_CHECK(m2->getH() == 30);
    BOOST_CHECK(m2->getDX() == -3);
    BOOST_CHECK(m2->getDY() == 3);

    m1->updateX(elapsedTime);
    BOOST_CHECK(m1->getX() == 10);
    m1->updateX(elapsedTime);
    BOOST_CHECK(m1->getX() == 20);
    m1->updateY(elapsedTime);
    BOOST_CHECK(m1->getY() == 0);

    m1->setDX(0);
    m1->updateX(elapsedTime);
    BOOST_CHECK(m1->getX() == 20);
    m1->setDY(50);
    m1->updateY(elapsedTime);
    BOOST_CHECK(m1->getY() == 50);


    m2->updateX(elapsedTime);
    BOOST_CHECK(m2->getX() == -53);
    m2->updateX(elapsedTime);
    BOOST_CHECK(m2->getX() == -56);
    m2->updateY(elapsedTime);
    BOOST_CHECK(m2->getY() == 53);
    m2->updateY(elapsedTime);
    BOOST_CHECK(m2->getY() == 56);

    delete m1;
    delete m2;

}

BOOST_AUTO_TEST_CASE ( test_Bullet )
{
    Ship * s1 = new Ship(0, 0, 15, 15, 10, 0, 100);

    Bullet * b1 = new Bullet(s1, 10, -7, 5, 40);
    BOOST_CHECK(b1->getX() == 10);
    BOOST_CHECK(b1->getY() == -7);
    BOOST_CHECK(b1->getW() == Bullet::DEF_BULLET_WIDTH);
    BOOST_CHECK(b1->getH() == Bullet::DEF_BULLET_HEIGHT);
    BOOST_CHECK(b1->getDX() == 5);
    BOOST_CHECK(b1->getDY() == 0);
    BOOST_CHECK(b1->getDamage() == 40);


    Bullet * b2 = new Bullet(s1, -10, 10);
    BOOST_CHECK(b2->getX() == s1->getX() - (int) (s1->getW())/2);
    BOOST_CHECK(b2->getY() == s1->getY() + (int) (s1->getH())/2);
    BOOST_CHECK(b2->getW() == Bullet::DEF_BULLET_WIDTH);
    BOOST_CHECK(b2->getH() == Bullet::DEF_BULLET_HEIGHT);
    BOOST_CHECK(b2->getDX() ==-10);
    BOOST_CHECK(b2->getDY() == 0);
    BOOST_CHECK(b2->getDamage() == 10);

    delete b1;
    delete s1;
    delete b2;
}

BOOST_AUTO_TEST_CASE( test_Ship )
{
    Player * s1  = new Player();

    Ship * s2  = new Ship(0, 0, 10, 0);
    BOOST_CHECK(s2->getX() == 0);
    BOOST_CHECK(s2->getY() == 0);
    BOOST_CHECK(s2->getW() == 30);
    BOOST_CHECK(s2->getH() == 30);
    BOOST_CHECK(s2->getDX() == 10);
    BOOST_CHECK(s2->getDY() == 0);
    BOOST_CHECK(s2->getHP() == 100);



    s2->updateHp(-99);
    BOOST_CHECK(s2->getHP() == 1);
    s2->updateHp(0);
    BOOST_CHECK(s2->getHP() == 1);
    s2->updateHp(100);
    BOOST_CHECK(s2->getHP() == 100);
    s2->updateHp(2);
    BOOST_CHECK(s2->getHP() == 100);

    BOOST_CHECK(s1->canShoot());
    Bullet * b = s1->shoot();
    BOOST_CHECK(b->getDX() == 700);
    //Vaisseau a tiré, besoin temppo pour re tirer
    BOOST_CHECK(!s1->canShoot());

    //test validité tempo
    int timeDeb = time(NULL);

    while(time(NULL) - timeDeb < ((float) Ship::DEF_FIRE_SPEED )/ 1000.f + 1);

    BOOST_CHECK(s1->canShoot());

    delete s2;
    delete s1;
}

BOOST_AUTO_TEST_CASE( test_Player )
{
    Player * p1 = new Player();
    BOOST_CHECK(p1->getX() == Player::START_POS_X);
    BOOST_CHECK(p1->getY() == Player::START_POS_Y);
    BOOST_CHECK(p1->getW() == Entity::DEF_W);
    BOOST_CHECK(p1->getH() == Entity::DEF_H);
    BOOST_CHECK(p1->getDX() == Ship::DEF_DX);
    BOOST_CHECK(p1->getDY() == 0);
    BOOST_CHECK(p1->getPoints() == 0);
    BOOST_CHECK(p1->getCash() == 0);
    BOOST_CHECK(p1->getBombsNumber() == Player::DEF_BOMB);
    BOOST_CHECK(!p1->hasDoubleBullets());
    BOOST_CHECK(!p1->hasDoubleFireSpeed());

    set<Enemy *> es;
    set<Item *> it;
    Enemy * e1 = new Enemy(Player::START_POS_X,  Player::START_POS_Y, 0, 0, 1, 0);
    Enemy * e2 = new Enemy(500, 0, 0, 0, 1, 0);

    es.insert(e1);
    es.insert(e2);

    p1->bomb(es, it);
    BOOST_CHECK(p1->getBombsNumber() == Player::DEF_BOMB - 1);
    BOOST_CHECK(e1->getHP() == 0);
    BOOST_CHECK(e2->getHP() == 100);

    p1->bomb(es, it);
    BOOST_CHECK(p1->getBombsNumber() == Player::DEF_BOMB - 2);
    BOOST_CHECK(e1->getHP() == 0);
    BOOST_CHECK(e2->getHP() == 100);

    p1->bomb(es, it);
    BOOST_CHECK(p1->getBombsNumber() == Player::DEF_BOMB - 3);
    BOOST_CHECK(e1->getHP() == 0);
    BOOST_CHECK(e2->getHP() == 100);

    p1->bomb(es, it);
    BOOST_CHECK(p1->getBombsNumber() == Player::DEF_BOMB - 3);
    BOOST_CHECK(e1->getHP() == 0);
    BOOST_CHECK(e2->getHP() == 100);

    p1->addPoints(e1->getPointsOnKill());
    BOOST_CHECK(p1->getPoints() == (int) e1->getPointsOnKill());
    p1->addPoints(e2->getPointsOnKill());
    BOOST_CHECK(p1->getPoints() == (int) e1->getPointsOnKill() + (int) e2->getPointsOnKill());

    p1->setDoubleBullets(true);
    p1->setDoubleFireSpeed(true);
    BOOST_CHECK(p1->hasDoubleBullets());
    BOOST_CHECK(p1->hasDoubleFireSpeed());
    p1->cancelBonuses();
    BOOST_CHECK(!p1->hasDoubleBullets());
    BOOST_CHECK(!p1->hasDoubleFireSpeed());


    p1->updateHp(-99);
    BOOST_CHECK(p1->getHP() == 1);
    BOOST_CHECK(p1->getLife() == 3);
    p1->updateHp(-1);
    BOOST_CHECK(p1->getHP() == 100);
    BOOST_CHECK(p1->getLife() == 2);
    p1->updateHp(100);
    BOOST_CHECK(p1->getHP() == 100);

    delete p1;
    delete e2;
}

BOOST_AUTO_TEST_CASE( test_Item )
{
    Player *s1 = new Player();
    s1->updateHp(-50);
    BOOST_CHECK(s1->getHP() == 50 );
    BOOST_CHECK(!s1->hasDoubleBullets());
    BOOST_CHECK(!s1->hasDoubleFireSpeed());

    Item * i1 = Item::HealthPackBonus(0, 0);
    BOOST_CHECK(i1->getType() == ItemType::HealthPack);
    i1->act(s1);
    BOOST_CHECK(s1->getHP() == 80 );

    Item * i2 = Item::DoubleBulletsBonus(0, 0);
    BOOST_CHECK(i2->getType() == ItemType::DoubleBullets);
    i2->act(s1);
    BOOST_CHECK(s1->hasDoubleBullets());

    Item * i3 = Item::DoubleFireRateBonus(0, 0);
    BOOST_CHECK(i3->getType() == ItemType::FireRate);
    i3->act(s1);
    BOOST_CHECK(s1->hasDoubleFireSpeed());

    delete s1;
    delete i1;
    delete i2;
    delete i3;


}

BOOST_AUTO_TEST_CASE( test_Enemy )
{
    unsigned int lvl1 = 1, lvl2 = 2;
    Ship s1(0, 0, 15, 15, 10, 0, 100);
    Enemy * e1 = new Enemy(0, -100, 10, 10, -10, 0, lvl1, lvl1);
    Enemy * e2 = new Enemy(0, 100, -10, 0, lvl2, lvl1 );
    Enemy * e3 = new Enemy(1000, 100, -3, 2, 1, lvl2);
    Enemy * e4 = new Enemy(0, 0, -3, 2, 1, 3);


    e1->moveToPlayerShip(s1, elapsedTime);
    BOOST_CHECK(e1->getDY() == Enemy::DEF_DY);
    e2->moveToPlayerShip(s1, elapsedTime);
    BOOST_CHECK(e2->getDY() == -Enemy::DEF_DY);
    e3->moveToPlayerShip(s1, elapsedTime);
    BOOST_CHECK(e3->getDY() == 0);
    e4->moveToPlayerShip(s1, elapsedTime);

    Bullet * b = e1->shoot();
    BOOST_CHECK(b->getDX() == -Bullet::DEF_BULLET_SPEED);

    BOOST_CHECK(e1->getPointsOnKill() == 10);
    BOOST_CHECK(e2->getPointsOnKill() == 15);

    delete e1;
    delete e2;
    delete e3;
    delete e4;
}


BOOST_AUTO_TEST_CASE( test_Level )
{
    Level * l = new Level(123456,1);
    std::set<Enemy*> * enemies;
    BOOST_CHECK( l->isFinished() == false );
    BOOST_CHECK( l->canGenerateEnemy() == true );

    int nb = l->getNumberOfEnemies();

    for (int i=0; i< nb; i++)
        l->generateEnemy(0);

    enemies=l->getEnemies();
    BOOST_CHECK( (int) l->getEnemies()->size() == nb);
    enemies->clear();
    BOOST_CHECK( l->isFinished());

    delete l;
}
