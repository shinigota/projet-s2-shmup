#include "Player.hpp"
#include "Ship.hpp"
#include "Enemy.hpp"
#include "Bullet.hpp"
#include "Item.hpp"
#include <iostream>

using namespace std;

Player::Player()
    : Ship(Player::START_POS_X, Player::START_POS_Y, Ship::DEF_DX, 0), _points(0),_cash(0), _bombs(Player::DEF_BOMB), _life(START_LIFE), _healthLevel(0), _fireRateLevel(0), _damageLevel(0), _doubleFireSpeed(false), _doubleBullets(false) {}

Player::~Player() {}

string Player::toString() const
{
    return "Player : " + Ship::toString() + " - point : " + to_string(_points) + " - cash : " + to_string(_cash) + " - bomb : " + to_string(_bombs) + " - doubleFireSpeed : " + to_string(_doubleFireSpeed) + " - doubleBullets : " + to_string(_doubleBullets);
}

bool Player::updateHp(int deltaHP)
{
    if(deltaHP < 0)
    {
        if(_hp + deltaHP <= 0)
        {
            _hp = Ship::START_HP + _healthLevel * 10;
            _life--;
        }
        else
        {
            _hp += deltaHP;
        }

        return (_life <= 0);
    }
    else if(_hp + deltaHP > Ship::START_HP + _healthLevel * 10)
    {
        _hp = Ship::START_HP + _healthLevel * 10;
    }
    else
    {
        _hp += deltaHP;
    }

    return false;
}

void Player::addPoints(int points)
{
    _points += points;
}

void Player::updateCash(int cash)
{
    _cash += cash;
}

bool Player::canShoot() const
{
    if(_doubleFireSpeed)
        return (_clockShoot.GetElapsedTime() * 1000 - _lastShoot)  >= (_fireSpeed/2 - _fireRateLevel * FIRE_RATE_UP_RATIO);

    return (_clockShoot.GetElapsedTime() * 1000- _lastShoot)  >= (_fireSpeed - _fireRateLevel * FIRE_RATE_UP_RATIO);
}

Bullet * Player::shoot()
{
    return Ship::shoot(Bullet::DEF_BULLET_SPEED, 100 + _damageLevel * 10);
}

void Player::doubleShoot(set<Bullet *> * bullets)
{
    bullets->insert(new Bullet(this, _x + (int) _w/2, _y, Bullet::DEF_BULLET_SPEED, 100));
    bullets->insert(new Bullet(this, _x + (int) _w/2, _y + _h - (int) Bullet::DEF_BULLET_HEIGHT, Bullet::DEF_BULLET_SPEED, 100));

    _lastShoot = _clockShoot.GetElapsedTime() * 1000;
}

bool Player::hasLost() const
{
    return((_life <= 0 && _hp <= 0 ) || _life < 0 );
}

bool Player::bomb(set<Enemy *> & enemies, set<Item *> & items)
{
    //si on a des bombes
    if(_bombs > 0)
    {
        _bombs --;
        //on vérifie que les ennemis sont dans une certaine distance, on leur inflige des dégâts
        for(auto e : enemies)
        {
            if(distance(*e) <= BOMB_RADIUS)
            {
                bool isDead = e->updateHp(- BOMB_DAMAGE - _bombDamageLevel * 25);
                if(isDead)
                {
                    Item * item = e->dropItem();
                    if(item != nullptr)
                            items.insert(item);

                    enemies.erase(e);
                    delete e;
                }
            }
        }
        return true;
    }
    return false;
}

void Player::addBomb()
{
    _bombs++;
}

void Player::cancelBonuses()
{
    _doubleBullets = false;
    _doubleFireSpeed = false;
}
//Getters
int Player::getPoints() const
{
    return _points;
}

int Player::getCash() const
{
    return _cash;
}

int Player::getBombsNumber() const
{
    return _bombs;
}

int Player::getBombPrice() const
{
    return BOMB_PRICE;
}

int Player::getLife() const
{
    return _life;
}

bool Player::hasDoubleBullets() const
{
    return _doubleBullets;
}

bool Player::hasDoubleFireSpeed() const
{
    return _doubleFireSpeed;
}

unsigned int Player::getMaxHealth() const
{
    return Ship::START_HP + _healthLevel * 10;
}

// Setters
void Player::setDoubleFireSpeed(bool state)
{
    _doubleFireSpeed = state;
}

void Player::setDoubleBullets(bool state)
{
    _doubleBullets = state;
}

void Player::upgradeHealth()
{
    _healthLevel++;
}

void Player::upgradeFireRate()
{
    _fireRateLevel++;
}

void Player::upgradeDamage()
{
    _damageLevel++;
}

void Player::upgradeBombDamage()
{
    _bombDamageLevel++;
}


int Player::getHealthUpgradePrice() const
{
    return 500 + _healthLevel * 150;
}

int Player::getFireRateUpgradePrice() const
{
    return 500 + _fireRateLevel * 150;
}

int Player::getDamageUpgradePrice() const
{
    return 500 + _damageLevel * 150;
}

int Player::getBombDamageUpgradePrice() const
{
    return 500 + _bombDamageLevel * 150;
}
