#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>
#include "Screen.hpp"

class GameScreen;
class Player;

class Game : public Screen {
private:
    sf::RenderWindow * _window;
    Screen * _screen;
    GameScreen * _gameScreen;
    bool _defaultGraphics, _play;
public:
    Game(sf::RenderWindow * window);
    ~Game();

    //------------------------------------------------------------------------------
    // Input: fps, the number of frame per second the screen will use to update (same update speed with fps)
    // Output:  /
    // Return:  /
    // Purpose: updating the current screen ( _screen )
    //------------------------------------------------------------------------------
    void update(float fps);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: drawing the current screen ( _screen )
    //------------------------------------------------------------------------------
    void draw();

    //------------------------------------------------------------------------------
    // Input: Screen * the new screen to render and update
    // Output:  /
    // Return:  /
    // Purpose: changing game's screen to draw and update (either menu, gamescreen, transitionscreen...
    //------------------------------------------------------------------------------
    void setScreen(Screen * screen);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: displaying game's RenderWindow
    //------------------------------------------------------------------------------
    void display();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: change _screen to _gameScreen so that the current screen that will be updated and rendered will be the game
    //------------------------------------------------------------------------------
    void backToGameScreen();

    //------------------------------------------------------------------------------
    // Input: int level, the number of the finished level
    // Output:  /
    // Return:  /
    // Purpose: set _screen to a transition screen that appears between two levels with "level (level) complete" text
    //------------------------------------------------------------------------------
    void transitionScreenNextLevel(int level);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: set screen to a gameover screen when the player dies
    //------------------------------------------------------------------------------
    void transitionScreenGameOver();


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  _windonw the game's window
    // Purpose: get game's window
    //------------------------------------------------------------------------------
    sf::RenderWindow * getWindow();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  true if defaultGraphics, false otherwise
    // Purpose: knowing which images to load
    //------------------------------------------------------------------------------
    bool defaultGraphics();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  _gameModel's player
    // Purpose: /
    //------------------------------------------------------------------------------
    Player * getPlayer();

    //------------------------------------------------------------------------------
    // Input: val : new defaultGraphic value
    // Output:  /
    // Return:  /
    // Purpose: change images to load
    //------------------------------------------------------------------------------
    void setDefaultGraphics(bool val);


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: delete current _gameScreen if it exists and create a new one (new game)
    //------------------------------------------------------------------------------
    void startGame();


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: set _play boolean to false to stop and destruct the game
    //------------------------------------------------------------------------------
    void stopGame();


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  state of _play
    // Purpose: knowing if the game has to run or not
    //------------------------------------------------------------------------------
    bool continuePlaying();
};

#endif // GAME_H
