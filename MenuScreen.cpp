#include "MenuScreen.hpp"

#include <iostream>

using namespace sf;
using namespace std;

MenuScreen::MenuScreen(RenderWindow * window, Game * game)
    : _game(game), _window(window)
{
    _textFont.LoadFromFile("fonts/forcedsquare.ttf");
    _title = sf::String("Shoot Them Up", _textFont, 100);
    _title.SetPosition(_window->GetWidth() / 2 - _title.GetRect().GetWidth() / 2, 20);
}

MenuScreen::~MenuScreen()
{
    for(Button * b : _buttons)
        delete b;
}

MenuScreen *
MenuScreen::menuMain(sf::RenderWindow * window, Game * game)
{
    MenuScreen * menu = new MenuScreen(window, game);

    Button * buttonStart = Button::ButtonStart(350, 200, 150, 50, "images/default/jouer.png", "images/default/jouer_hover.png");
    Button * buttonOption = Button::ButtonOption(350, 300, 150, 50, "images/default/options.png", "images/default/options_hover.png");
    Button * buttonCredits = Button::ButtonCredits(350, 400, 150, 50, "images/default/credits.png", "images/default/credits_hover.png");
    Button * buttonQuit = Button::ButtonQuit(350, 500, 150, 50, "images/default/quit.png", "images/default/quit_hover.png");

    menu->addButton(buttonStart);
    menu->addButton(buttonOption);
    menu->addButton(buttonCredits);
    menu->addButton(buttonQuit);

    return menu;
}

MenuScreen *
MenuScreen::menuOptions(sf::RenderWindow * window, Game * game)
{
    MenuScreen * menu = new MenuScreen( window, game);

    Button * buttonGraphics;
    if(game->defaultGraphics())
        buttonGraphics = Button::ButtonGraphics(350, 300, 150, 50, "images/default/graphics_def.png", "images/default/graphics_def_hover.png");
    else
        buttonGraphics = Button::ButtonGraphics(350, 300, 150, 50, "images/default/graphics_var.png", "images/default/graphics_var_hover.png");
    Button * buttonMain = Button::ButtonMainMenu(350, 500, 150, 50, "images/default/menu.png", "images/default/menu_hover.png");

    menu->addButton(buttonGraphics);
    menu->addButton(buttonMain);
    return menu;
}


void MenuScreen::addButton(Button * b)
{
    _buttons.insert(b);
}



void
MenuScreen::draw()
{
    _window->Clear(sf::Color::Black);
    _window->Draw(_title);

    for(Button * b : _buttons)
        b->draw(_window);
}

void
MenuScreen::update(float fps)
{
    for (Button * b : _buttons)
            b->setHover(b->contains(_window->GetInput().GetMouseX(), _window->GetInput().GetMouseY()));

    sf::Event ev;
    while (_window->GetEvent(ev))
    {
        if((ev.Type==sf::Event::MouseButtonPressed) && (ev.MouseButton.Button==Mouse::Left))
        {
            bool hasClicked = false;
            for (auto b = _buttons.begin(); b != _buttons.end() && !hasClicked; b++)
            {
               if((*b)->contains(ev.MouseButton.X,ev.MouseButton.Y))
               {
                   (*b)->act(_game);
                    hasClicked = true;
               }

            }
        }
    }
}


std::set<Button *>
MenuScreen::getButtons()
{
    return _buttons;
}
