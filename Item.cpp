#include "Item.hpp"
#include "Player.hpp"

#include <iostream>

using namespace std;

Item::Item(int x, int y, ItemType type, function<void(Player *)> action)
    : Entity(x, y), _itemType(type), _action(action) {}

Item * Item::DoubleFireRateBonus(int x, int y)
{
    function<void(Player *)> action = [](Player * p)
    {
        p->setDoubleFireSpeed(true);
    };
    return new Item(x, y, FireRate, action);
}

Item * Item::DoubleBulletsBonus(int x, int y)
{
    function<void(Player *)> action = [](Player * p)
    {
        p->setDoubleBullets(true);
    };
    return new Item(x, y, DoubleBullets, action);
}

Item * Item::HealthPackBonus(int x, int y)
{
    function<void(Player *)> action = [](Player * p)
    {
        p->updateHp(Item::HP_BONUS);
    };
    return new Item(x, y, HealthPack, action);
}

Item * Item::CoinBonus(int x, int y)
{
    function<void(Player *)> action = [](Player * p)
    {
        p->updateCash(485);
    };
    return new Item(x, y, Coin, action);
}

Item * Item::BombBonus(int x, int y)
{
    function<void(Player *)> action = [](Player * p)
    {
        p->addBomb();
    };
    return new Item(x, y, Bomb, action);
}

string Item::toString() const
{
    string type;
    switch(_itemType)
    {
    case Coin:
        type = "Coin";
        break;
    case DoubleBullets:
        type = "DoubleBullets";
        break;
    case FireRate:
        type = "FireRate";
        break;
    case HealthPack:
        type = "HealthPack";
        break;
    case Bomb:
        type = "Bomb";
        break;
    default :
        type = "null";
        break;
    }
    return "Item : - ItemType : " + type + " - " + Entity::toString();
}

void Item::act(Player * p) const
{
    _action(p);
}

ItemType Item::getType() const
{
    return _itemType;
}
