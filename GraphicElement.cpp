#include "GraphicElement.hpp"

using namespace sf;
GraphicElement::GraphicElement()
    : Sprite(), _w(0), _h(0), _x(0)
{}

GraphicElement::GraphicElement(sf::Image * image, int x, int y, int w, int h, float rotation)
    : Sprite(*image), _w(w), _h(h), _x(x), _y(y)
{
    resize(w, h);
    SetCenter(GetImage()->GetWidth() / 2, GetImage()->GetHeight() / 2);
    SetRotation(rotation);
    SetPosition(x + _w/2, y + _h/2);
}

GraphicElement::~GraphicElement()
{}

void GraphicElement::draw(float xOrigin, sf::RenderWindow * window)
{
    SetPosition(_x - xOrigin, _y);
    window->Draw(*this);

}

void GraphicElement::setPosition(int x, int y)
{
    _x = x + _w/2;
    _y = y + _h/2;
}

void GraphicElement::setPosition(const sf::Vector2f & pos)
{
    _x = pos.x + _w/2;
    _y = pos.y + _h/2;

}

void GraphicElement::resize(int w, int h)
{
    _w = w;
    _h = h;
    Resize(_w, _h);
}
