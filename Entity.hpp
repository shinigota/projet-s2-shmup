#ifndef _ENTITY_H_
#define _ENTITY_H_

#include <string>
#include <cstdlib>

class Entity {
protected:
    float _x, _y;
    unsigned int _w, _h;
public:
    static const unsigned int DEF_W=30, DEF_H=30;

    Entity(float x, float y, unsigned int w, unsigned int h);
    Entity(float x, float y);
    Entity();
    virtual ~Entity();

    //------------------------------------------------------------------------------
    // Input: entity : the entity to check if the current object collides or not
    // Output:  true if the 2 entities collides, esle false
    // Purpose: detecting collisions
    //------------------------------------------------------------------------------
    bool collides(const Entity & e) const;

    //------------------------------------------------------------------------------
    // Input: xOrigin  : the origin used to compare the current entity's pos, screenWidth screenHeight the dimensions of the screen
    // Output:  true if the entity is out of the top of the screen, the bottom or the left, else false
    // Purpose: detecting if an entity is out of the screen so it can be deleted
    //------------------------------------------------------------------------------
    bool outOfScreen(float xOrigin, unsigned int screenWidth, unsigned int screenHeight) const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  string containing entity's informations
    // Purpose: display text version of an entity
    //------------------------------------------------------------------------------
    virtual std::string toString() const;

    //------------------------------------------------------------------------------
    // Input: entity : the entity used to calculate the distance
    // Output:  /
    // Return:  distance between the two entities
    // Purpose: knowing distance between two entities (used for bomb range)
    //------------------------------------------------------------------------------
    float distance(const Entity & e) const;

    //Getters

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  x position of the entity
    // Purpose: knowing the x position of the entity
    //------------------------------------------------------------------------------
    float getX() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  y position of the entity
    // Purpose: knowing the y position of the entity
    //------------------------------------------------------------------------------
    float getY() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  width of the entity
    // Purpose: knowing the width of the entity
    //------------------------------------------------------------------------------
    unsigned int getW() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  height of the entity
    // Purpose: knowing the height of the entity
    //------------------------------------------------------------------------------
    unsigned int getH() const;

    //Setters
    //------------------------------------------------------------------------------
    // Input: x the new position
    // Output:  /
    // Return:  /
    // Purpose: moving the x coordinate of the entity
    //------------------------------------------------------------------------------
    void setX(float x);

    //------------------------------------------------------------------------------
    // Input: y the new position
    // Output:  /
    // Return:  /
    // Purpose: moving the y coordinate of the entity
    //------------------------------------------------------------------------------
    void setY(float y);
};
#endif // _ENTITY_H_
