#include "Button.hpp"
#include "GameView.hpp"
#include "SFML/Graphics.hpp"
#include "Game.hpp"
#include "MenuScreen.hpp"
#include "CreditScreen.hpp"

using namespace std;
using namespace sf;



Button::Button(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover, function<void(Game *)> action)
    :_x(x), _y(y), _w(w), _h(h), _img(Image()), _sprite(Sprite()), _action(action)
{
    if(_img.LoadFromFile(path))
        _sprite = Sprite(_img);
    if(_imgHover.LoadFromFile(pathHover))
        _sprite_hover= Sprite(_imgHover);

    _sprite.SetPosition(x, y);
    _sprite.Resize(w, h);

    _sprite_hover.SetPosition(x, y);
    _sprite_hover.Resize(w, h);
}

Button::Button(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
    :_x(x), _y(y), _w(w), _h(h), _img(Image()), _sprite(Sprite())
{
    if(_img.LoadFromFile(path))
        _sprite = Sprite(_img);
    if(_imgHover.LoadFromFile(pathHover))
        _sprite_hover= Sprite(_imgHover);

    _sprite.SetPosition(x, y);
    _sprite.Resize(w, h);

    _sprite_hover.SetPosition(x, y);
    _sprite_hover.Resize(w, h);
}


Button::~Button()
{}

Button * Button::ButtonStart(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    //nouveau jeu
    function<void(Game *)> action = [](Game * g)
    {
        g->startGame();
    };
    return new Button(x, y, w, h, path, pathHover, action);
}

Button * Button::ButtonContinue(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    //retour au jeu
    function<void(Game *)> action = [](Game * g)
    {
        g->backToGameScreen();
    };
    return new Button(x, y, w, h, path, pathHover, action);
}

Button * Button::ButtonOption(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    //menu options
    function<void(Game *)> action = [](Game * g)
    {
        g->setScreen(MenuScreen::menuOptions(g->getWindow(), g));
    };
    return new Button(x, y, w, h, path, pathHover, action);
}

Button * Button::ButtonCredits(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    //menu crédits
    function<void(Game *)> action = [](Game * g)
    {
        g->setScreen(new CreditScreen(g->getWindow(), g));
    };
    return new Button(x, y, w, h, path, pathHover, action);
}

Button * Button::ButtonQuit(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    //fermeture jeu
    function<void(Game *)> action = [](Game * g)
    {
        g->stopGame();
    };
    return new Button(x, y, w, h, path, pathHover, action);
}

Button * Button::ButtonGraphics(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    Button * b = new Button(x, y, w, h, path, pathHover);
    //changement charte graphique
    function<void(Game *)> action = [b](Game * g)
    {
        g->setDefaultGraphics(!g->defaultGraphics());

        if(g->defaultGraphics())
            b->setImages("images/default/graphics_def.png", "images/default/graphics_def_hover.png");
        else
            b->setImages("images/default/graphics_var.png", "images/default/graphics_var_hover.png");
    };

    b->setAction(action);
     return b;
}



Button * Button::ButtonDamageUp(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    //achat amélioration dégats tirs
    function<void(Game *)> action = [](Game * g)
    {
        if (g->getPlayer()->getCash() >= g->getPlayer()->getDamageUpgradePrice() )
        {
            g->getPlayer()->upgradeDamage();
            g->getPlayer()->updateCash(-g->getPlayer()->getDamageUpgradePrice());
        }
    };
    return new Button(x, y, w, h, path, pathHover, action);
}



Button * Button::ButtonUpFireSpeed(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    //achat amélioration cadence de tir
    function<void(Game *)> action = [](Game * g)
    {
        if (g->getPlayer()->getCash() >= g->getPlayer()->getFireRateUpgradePrice() )
        {
            g->getPlayer()->upgradeFireRate();
            g->getPlayer()->updateCash(-g->getPlayer()->getFireRateUpgradePrice());
        }
    };
    return new Button(x, y, w, h, path, pathHover, action);
}


Button * Button::ButtonLifeUp(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    //achat amélioration vie
    function<void(Game *)> action = [](Game * g)
    {
        if (g->getPlayer()->getCash() >= g->getPlayer()->getHealthUpgradePrice() )
        {
            g->getPlayer()->upgradeHealth();
            g->getPlayer()->updateHp(g->getPlayer()->getMaxHealth());
            g->getPlayer()->updateCash(-g->getPlayer()->getHealthUpgradePrice());
        }
    };
    return new Button(x, y, w, h, path, pathHover, action);
}

Button * Button::ButtonUpBombPower(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    //achat amélioration dégâts bombes
    function<void(Game *)> action = [](Game * g)
    {
        if (g->getPlayer()->getCash() >= g->getPlayer()->getBombDamageUpgradePrice() )
        {
            g->getPlayer()->upgradeBombDamage();
            g->getPlayer()->updateCash(-g->getPlayer()->getBombDamageUpgradePrice());
        }
    };
    return new Button(x, y, w, h, path, pathHover, action);
}

Button * Button::ButtonBuyBomb(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    //achat bombe
     function<void(Game *)> action = [](Game * g)
    {
        if (g->getPlayer()->getCash() >= g->getPlayer()->getBombPrice() )
        {
            g->getPlayer()->addBomb();
            g->getPlayer()->updateCash(-g->getPlayer()->getBombPrice());
        }
    };
    return new Button(x, y, w, h, path, pathHover, action);
}

Button * Button::ButtonMainMenu(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover)
{
    //menu principal
    function<void(Game *)> action = [](Game * g)
    {
        g->setScreen(MenuScreen::menuMain(g->getWindow(), g));
    };
    return new Button(x, y, w, h, path, pathHover, action);
}



bool Button::contains(int x, int y) const
{
    return ! (x > _x + (int) _w || x < _x || y > _y + (int) _h || y < _y);
}

void Button::draw(sf::RenderWindow * window)
{
    if(!_hover)
        window->Draw(_sprite);
    else
        window->Draw(_sprite_hover);
}

//Getters
int Button::getX() const
{
    return _x;
}

int Button::getY() const
{
    return _y;
}

unsigned int Button::getW() const
{
    return _w;
}

unsigned int Button::getH() const
{
    return _h;
}

void Button::setHover(bool hover)
{
    _hover=hover;
}

void Button::act(Game * g) const
{
    _action(g);
}

void Button::setImages(string normalPath, string hoverPath)
{
    int x = _x,
        y = _y,
        w = _w,
        h = _h;

    if(_img.LoadFromFile(normalPath))
        _sprite = Sprite(_img);
    if(_imgHover.LoadFromFile(hoverPath))
        _sprite_hover= Sprite(_imgHover);

    _sprite.SetPosition(x, y);
    _sprite.Resize(w, h);

    _sprite_hover.SetPosition(x, y);
    _sprite_hover.Resize(w, h);
}

void Button::setAction(std::function<void(Game *)> action)
{
    _action = action;
}
