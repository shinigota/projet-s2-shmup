#include <iostream>
#include <cstdlib>
#include "GameModel.hpp"
#include "Enemy.hpp"
#include "Player.hpp"
#include "Bullet.hpp"
#include "Item.hpp"
#include "Level.hpp"
#include "Game.hpp"

using namespace std;

GameModel::GameModel(Game * game)
    :_w(GameView::DEF_SCREEN_W), _h(GameView::DEF_SCREEN_H), _seed(rand()), _currentDifficulty(1), _game(game), _level(new Level(_seed, 1)), _enemies(_level->getEnemies()), _bullets(), _items(), _player(new Player()), _lastUpdate(0), _fps(0)
{
    srand(_seed);
}

GameModel::~GameModel()
{
    clearAll();


    delete _level;
    delete _player;
}


void GameModel::clearAll()
{
    for(Bullet * b : _bullets)
        delete b;
    _bullets.clear();

    for(Item * i : _items)
        delete i;

    _items.clear();
}

void GameModel::nextStep(int fps)
{
    float elapsedTime  = 1.0f/(float) fps;

    _player->updateX(elapsedTime);
    if(_player->getY() + _player->getH() < _h && _player->getDY() > 0)
        _player->updateY(elapsedTime);
    else if(_player->getY() > 0 && _player->getDY() < 0)
        _player->updateY(elapsedTime);
    else if(_player->getY() < 0)
        _player->setY(0);
    else if(_player->getY() + _player->getH() > _h)
        _player->setY(_h - _player->getH());

    if(_level->canGenerateEnemy())
        _level->generateEnemy(_player->getX());

    //Parcours des ennemis
    _enemies = _level->getEnemies();

    for(Enemy * e : *_enemies)
    {

        //Si il est en dehors de l'écran on le supprime
        if(e->outOfScreen(_player->getX(), _w, _h))
        {
            delete e;
            _enemies->erase(e);

        }
        else
        {
            //on actualise les déplacements de l'ennemi
            e->moveToPlayerShip(*_player, elapsedTime);
            e->updateX(elapsedTime);
            e->updateY(elapsedTime);

            //tir de l'ennemi
            if(e->canShoot(*_player))
                _bullets.insert(e->shoot());

            if(e->collides(*_player))
            {
                _player->updateHp(- _player->getMaxHealth());
                delete e;
                _enemies->erase(e);
            }


        }
    }

    //parcours des projectiles
    bool deleted = false;
    for(Bullet * b : _bullets)
    {
        //si en dehors de l'écran, on le détruit
        if(b->outOfScreen(_player->getX(), _w, _h))
        {
            delete b;
            _bullets.erase(b);
            deleted = true;
        }
        else if(b->isEnemy() && _player->collides(*b)) //sinon si collision avec joueur et tir ennemi
        {
            //annulation bonus, maj vie
            _player->updateHp(- b->getDamage());


            //_player->cancelBonuses();

            delete b;
            _bullets.erase(b);
            deleted = true;
        }
        else if(!b->isEnemy())
        {

            for(auto it = _enemies->begin(); it != _enemies->end() && !deleted; it++)
            {
                Enemy * e = *it;
                //Si il y a collision avec un ennemi et que tir joueur
                if(e->collides(*b))
                {
                    if(e->updateHp(-(b->getDamage()))) //si la mise a jour de la vie provoque la mort de l'ennemi
                    {
                        //mise à jour du score et création du bonus
                        _player->addPoints(e->getPointsOnKill());
                        Item * item = e->dropItem();

                        //si un bonus a été généré (!= null) on l'ajoute
                        if(item != nullptr)
                            _items.insert(item);

                        delete e;
                        _enemies->erase(e);
                        e = nullptr;

                        deleted = true;
                    }

                    delete b;
                    _bullets.erase(b);

                }

            /*    if(deleted)
                    break;*/
            }

        }

        if(!deleted)
        {
            b->updateX(elapsedTime);
        }

    }

    //parcours des items
    for(auto it = _items.begin(); it != _items.end(); it++)
    {
        //destruction si en dehors
        if((*it)->outOfScreen(_player->getX(), _w, _h))
        {
            delete (*it);
            _items.erase(it);
        }
        else if((*it)->collides(*_player))
        {
            (*it)->act(_player);

            delete (*it);
            _items.erase(it);
        }
    }



    if(gameEnded())
    {
        clearAll();
        _game->transitionScreenGameOver();
    }
    else if (_level->isFinished())
    {
        //On vide le niveau
        clearAll();

        //on passe à la boutique
        _game->transitionScreenNextLevel(_currentDifficulty);

        //on change le seed pour créer un niveau différent
        _seed*=2;
        _level=new Level(_seed, ++_currentDifficulty);

        //on réinitialise la vie et la position du joueur
        _player->setX(Player::START_POS_X);
        _player->setY(Player::START_POS_Y);
        _player->updateHp(_player->getMaxHealth());
    }

}

set<Enemy*> * GameModel::getEnemies()
{
    return _level->getEnemies();
}

set<Bullet*> * GameModel::getBullets()
{
    return &_bullets;
}

set<Item*> * GameModel::getItems()
{
    return &_items;
}

Level * GameModel::getLevel()
{
    return _level;
}

Player * GameModel::getPlayer()
{
    return _player;
}

bool GameModel::gameEnded()
{
    return _player->hasLost();
}

set<MovableEntity *> GameModel::getEntities()
{
    set<MovableEntity *> res;
    for(Enemy * e : *_enemies)
        res.insert(e);

    res.insert(_bullets.begin(), _bullets.end());
    res.insert(_player);

    return res;
}
