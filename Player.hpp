#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <set>
#include <string>
#include "Ship.hpp"
#include "GameView.hpp"

class Enemy;
class Item;

class Player : public Ship
{
private:
    int _points, _cash, _bombs, _life, _healthLevel, _fireRateLevel, _damageLevel, _bombDamageLevel;
    bool _doubleFireSpeed, _doubleBullets;
public:
    static const int BOMB_PRICE = 500, DEF_BOMB = 3, START_POS_X = 20, START_POS_Y=GameView::DEF_SCREEN_H / 2 - Ship::DEF_H / 2,
                BOMB_RADIUS = 500, BOMB_DAMAGE = 100, START_LIFE = 3, SLOW_SPEED = Ship::DEF_DX/2, NORMAL_SPEED = Ship::DEF_DX,
                FAST_SPEED = 2 * Ship::DEF_DX, FIRE_RATE_UP_RATIO = 20;
    Player();
    ~Player();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  string containing player's informations
    // Purpose: display text version of a player's
    //------------------------------------------------------------------------------
    std::string toString() const override;

    //------------------------------------------------------------------------------
    // Input: deltaHP : amount of HP (positive or negative) to add to the player
    // Output:  /
    // Return:  true if ship's life reaches 0 (no more life, dead) , otherwise false
    // Purpose: updating an ship's health and life points and knowing if he's dead
    //------------------------------------------------------------------------------
    virtual bool updateHp(int deltaHP);

    //------------------------------------------------------------------------------
    // Input: points : amount of points (positive or negative) to add to the player
    // Output:  /
    // Return:  /
    // Purpose: updating player's points
    //------------------------------------------------------------------------------
    void addPoints(int points);

    //------------------------------------------------------------------------------
    // Input: cash : amount of cash (positive or negative) to add to the player
    // Output:  /
    // Return:  /
    // Purpose: updating player's cash
    //------------------------------------------------------------------------------
    void updateCash(int cash);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  true if the player can shoot, false else
    // Purpose: knowing, according to the date of the last shot and the fire rate (or double fire rate with a bonus) if the player can shoot or no
    //------------------------------------------------------------------------------
    bool canShoot() const override;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  true if the has no HP and life
    // Purpose: if a player is completly dead or no
    //------------------------------------------------------------------------------
    bool hasLost() const;


    //------------------------------------------------------------------------------
    // Input: enemies, the set of enemy the bomb will modify enemy's life or no, items, the set of items in which some bonuses will be added or no
    // Output:  /
    // Return:  true if the player used a bomb, false if he failed.
    // Purpose: bombing if the player has bombs left
    //------------------------------------------------------------------------------
    bool bomb(std::set<Enemy *> & enemies, std::set<Item *> & items);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: add one bomb to the player
    //------------------------------------------------------------------------------
    void addBomb();


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  new bullet
    // Purpose: creating a new bullet with the correct speed and current player's position
    //------------------------------------------------------------------------------
    Bullet* shoot();

    //------------------------------------------------------------------------------
    // Input: bullets, the set in which the player will add bullets
    // Output:  bullets, the set with 2 new bullets
    // Return:  /
    // Purpose: shooting 2 bullets at the same time
    //------------------------------------------------------------------------------
    void doubleShoot(std::set<Bullet *> * bullets);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: setting all the bonuses's booleans to false so that the player don't have any bonus left
    //------------------------------------------------------------------------------
    void cancelBonuses();

    //Getters
    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  the points of the player
    // Purpose: knowing player's points
    //------------------------------------------------------------------------------
    int getPoints() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  player's amount of cash
    // Purpose: knowing player's cash
    //------------------------------------------------------------------------------
    int getCash() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  the number of bombs left the player has
    // Purpose: knowing player's quantity of bombs left
    //------------------------------------------------------------------------------
    int getBombsNumber() const;

    int getBombPrice() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  number of lives left (between 0 and 3)
    // Purpose: knowing ship's lives left
    //------------------------------------------------------------------------------
    int getLife() const;
    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  state of double bullet bonus
    // Purpose: knowing if double bullet bonus is activated or no
    //------------------------------------------------------------------------------
    bool hasDoubleBullets() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  state of double bullet fire rate
    // Purpose: knowing if double bullet fire rate is activated or no
    //------------------------------------------------------------------------------
    bool hasDoubleFireSpeed() const;


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  the max health of the player
    // Purpose: knowing player's max health
    //------------------------------------------------------------------------------
    virtual unsigned int getMaxHealth() const;

    //Setters

    //------------------------------------------------------------------------------
    // Input: state, the new double bullets bonus state
    // Output:  /
    // Return:  /
    // Purpose: changing bonus state (enabling os disabling)
    //------------------------------------------------------------------------------
    void setDoubleBullets(bool state);

    //------------------------------------------------------------------------------
    // Input: state, the new double fire rate bonus state
    // Output:  /
    // Return:  /
    // Purpose: changing double fire rate bonus state (enabling os disabling)
    //------------------------------------------------------------------------------
    void setDoubleFireSpeed(bool state);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: upgrading player's max health
    //------------------------------------------------------------------------------
    void upgradeHealth();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: upgrading player's fire rate
    //------------------------------------------------------------------------------
    void upgradeFireRate();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: upgrading player's damage
    //------------------------------------------------------------------------------
    void upgradeDamage();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: upgrading player's bomb damage
    //------------------------------------------------------------------------------
    void upgradeBombDamage();


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return: an int which is the price of the next health upgrade
    // Purpose: knowing the next upgrade's price
    //------------------------------------------------------------------------------
    int getHealthUpgradePrice() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return: an int which is the price of the next fire rate upgrade
    // Purpose: knowing the next upgrade's price
    //------------------------------------------------------------------------------
    int getFireRateUpgradePrice() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return: an int which is the price of the next damage upgrade
    // Purpose: knowing the next upgrade's price
    //------------------------------------------------------------------------------
    int getDamageUpgradePrice() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return: an int which is the price of the next bomb damage upgrade
    // Purpose: knowing the next upgrade's price
    //------------------------------------------------------------------------------
    int getBombDamageUpgradePrice() const;
};

#endif //PLAYER_HPP

