#include "Screen.hpp"
#include <SFML/Graphics.hpp>

class Game;

class StartScreen : public Screen
{
private:
    Game * _game;
    sf::RenderWindow * _window;
    sf::Font _textFont;
    sf::String _title;
    sf::Clock _moveClock;
    bool _firstMoveDone;
public:
    StartScreen(sf::RenderWindow * window, Game * game);
    virtual ~StartScreen();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: drawing the screen
    //------------------------------------------------------------------------------
    void draw() override;

    //------------------------------------------------------------------------------
    // Input: fps, the number of frame per second the screen will use to update (same update speed with fps)
    // Output:  /
    // Return:  /
    // Purpose: updating the current screen
    //------------------------------------------------------------------------------
    void update(float fps) override;
};
