#ifndef CREDITSCREEN_H
#define CREDITSCREEN_H

#include <SFML/Graphics.hpp>
#include "MenuScreen.hpp"


class CreditScreen : public MenuScreen
{
private:
    std::set<sf::String *> _credits;

public:
    CreditScreen(sf::RenderWindow * window, Game * game);
    virtual ~CreditScreen();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: drawing the CreditScreen
    //------------------------------------------------------------------------------
    void draw() override;

    //------------------------------------------------------------------------------
    // Input: fps, the number of frame per second the current screen will use to update
    // Output:  /
    // Return:  /
    // Purpose: updating the current CreditScreen
    //------------------------------------------------------------------------------
    void update(float fps) override;


};

#endif // CREDITSCREEN_H
