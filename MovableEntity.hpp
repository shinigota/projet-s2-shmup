#ifndef MOVABLEENTITY_HPP
#define MOVABLEENTITY_HPP

#include "Entity.hpp"

class MovableEntity : public Entity {
protected:
    float _dx, _dy;
public:
    MovableEntity(float x, float y, unsigned int w, unsigned int h, float dx, float dy);
    MovableEntity(float x, float y, float dx, float dy);
    MovableEntity();
    ~MovableEntity() override;

    //------------------------------------------------------------------------------
    // Input: elapsedTime the time since last update (different FPS, same speed)
    // Output:  /
    // Return:  /
    // Purpose: updating the x position by adding the x speed
    //------------------------------------------------------------------------------
    void updateX(float elapsedTime);

    //------------------------------------------------------------------------------
    // Input: elapsedTime the time since last update (different FPS, same speed)
    // Output:  /
    // Return:  /
    // Purpose: updating the y position by adding the y speed
    //------------------------------------------------------------------------------
    void updateY(float elapsedTime);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  string containing movable entity's informations
    // Purpose: display text version of a movable entity
    //------------------------------------------------------------------------------
    std::string toString() const override;

    //Getters
    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  x speed of the movable entity
    // Purpose: knowing the x speed of the movable entity
    //------------------------------------------------------------------------------
    float getDX() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  y speed of the movable entity
    // Purpose: knowing the y speed of the movable entity
    //------------------------------------------------------------------------------
    float getDY() const;


    //Setters
    //------------------------------------------------------------------------------
    // Input: dx the new x speed
    // Output:  /
    // Return:  /
    // Purpose: changing the x speed of the movable entity
    //------------------------------------------------------------------------------
    void setDX(float dx);

    //------------------------------------------------------------------------------
    // Input: dy the new y peed
    // Output:  /
    // Return:  /
    // Purpose: changing the y speed of the movable entity
    //------------------------------------------------------------------------------
    void setDY(float dy);

};
#endif // MovableEntity_HPP

