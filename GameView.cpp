#include "GameView.hpp"
#include "GameModel.hpp"
#include "Game.hpp"
#include "Enemy.hpp"
#include "Bullet.hpp"
#include "Item.hpp"
#include "Level.hpp"
#include "Player.hpp"
#include "GraphicElement.hpp"
#include "MenuScreen.hpp"
#include <iostream>


using namespace std;
using namespace sf;


const std::string GameView::VARIANT_BG = "images/variant/bg.png";
const std::string GameView::DEFAULT_BG = "images/default/bg.png";
const std::string GameView::VARIANT_SHIP_PLAYER = "images/variant/playership.png";
const std::string GameView::VARIANT_SHIP1 = "images/variant/ship1.png";
const std::string GameView::VARIANT_SHIP2 = "images/variant/ship2.png";
const std::string GameView::VARIANT_SHIP3 = "images/variant/ship3.png";
const std::string GameView::DEFAULT_SHIP_PLAYER = "images/default/playership.png";
const std::string GameView::DEFAULT_SHIP1 = "images/default/ship1.png";
const std::string GameView::DEFAULT_SHIP2 = "images/default/ship2.png";
const std::string GameView::DEFAULT_SHIP3 = "images/default/ship3.png";
const std::string GameView::VARIANT_BULLET1 = "images/variant/bullet1.png";
const std::string GameView::VARIANT_BULLET2 = "images/variant/bullet2.png";
const std::string GameView::VARIANT_BULLET3 = "images/variant/bullet3.png";
const std::string GameView::DEFAULT_BULLET1 = "images/default/bullet.png";
const std::string GameView::DEFAULT_BULLET2 = "images/default/bullet.png";
const std::string GameView::DEFAULT_BULLET3 = "images/default/bullet.png";
const std::string GameView::VARIANT_HPACK = "images/variant/hPack.png";
const std::string GameView::VARIANT_COIN = "images/variant/coin.png";
const std::string GameView::VARIANT_DOUBLEFIRE = "images/variant/doublefire.png";
const std::string GameView::VARIANT_FIRERATE = "images/variant/firerate.png";
const std::string GameView::VARIANT_BOMBBONUS = "images/variant/bombbonus.png";
const std::string GameView::VARIANT_BOMB = "images/variant/bomb.png";
const std::string GameView::VARIANT_HEART = "images/variant/heart.png";
const std::string GameView::DEFAULT_HEART = "images/default/revive.png";
const std::string GameView::VARIANT_THRUSTSLOW = "images/variant/slow.png";
const std::string GameView::VARIANT_THRUSTNORMAL = "images/variant/normal.png";
const std::string GameView::VARIANT_THRUSTFAST = "images/variant/fast.png";

GameView::GameView(Game * game, bool defaultGraphics, GameModel * model)
    :_game(game), _model(model), _w(game->getWindow()->GetWidth()), _h(game->getWindow()->GetHeight()), _window(game->getWindow()), usedBomb(false)
{
    scoreFont.LoadFromFile("fonts/forcedsquare.ttf");
    scoreText = String("SCORE : 0", scoreFont, 30);
    cashText = String("CASH : 0$", scoreFont, 30);
    scoreText.Move(20, 120);
    cashText.Move(20, 160);
    initSprites(defaultGraphics);
}

GameView::~GameView()
{
    for(auto ge : _elementToGraphicElement)
        delete _elementToGraphicElement[ge.first];

    for(auto ge : _itemToGraphicElement)
        delete _itemToGraphicElement[ge.first];

    _elementToGraphicElement.clear();
    _itemToGraphicElement.clear();

}

void GameView::initSprites(bool defaultGraphics)
{
    //chargement des images communes
    _hpack_image.LoadFromFile(VARIANT_HPACK);
    _coin_image.LoadFromFile(VARIANT_COIN);
    _doublefire_image.LoadFromFile(VARIANT_DOUBLEFIRE);
    _firerate_image.LoadFromFile(VARIANT_FIRERATE);
    _bomb_bonus_image.LoadFromFile(VARIANT_BOMBBONUS);
    _bomb_image.LoadFromFile(VARIANT_BOMB);
    _thrust_fast_image.LoadFromFile(VARIANT_THRUSTFAST);
    _thrust_normal_image.LoadFromFile(VARIANT_THRUSTNORMAL);
    _thrust_slow_image.LoadFromFile(VARIANT_THRUSTSLOW);


    //chargement des images selon la charte choisie
    if(defaultGraphics)
    {
        _background_image.LoadFromFile(DEFAULT_BG);
        _ship_player_image.LoadFromFile(DEFAULT_SHIP_PLAYER);
        _ship_1_image.LoadFromFile(DEFAULT_SHIP1);
        _ship_2_image.LoadFromFile(DEFAULT_SHIP2);
        _ship_3_image.LoadFromFile(DEFAULT_SHIP3);
        _bullet_1_image.LoadFromFile(DEFAULT_BULLET1);
        _bullet_2_image.LoadFromFile(DEFAULT_BULLET2);
        _bullet_3_image.LoadFromFile(DEFAULT_BULLET3);
        _heart_image.LoadFromFile(DEFAULT_HEART);

        scoreText.SetColor(sf::Color::Black);
        cashText.SetColor(sf::Color::Black);
    }
    else
    {
        _background_image.LoadFromFile(VARIANT_BG);
        _ship_player_image.LoadFromFile(VARIANT_SHIP_PLAYER);
        _ship_1_image.LoadFromFile(VARIANT_SHIP1);
        _ship_2_image.LoadFromFile(VARIANT_SHIP2);
        _ship_3_image.LoadFromFile(VARIANT_SHIP3);
        _bullet_1_image.LoadFromFile(VARIANT_BULLET1);
        _bullet_2_image.LoadFromFile(VARIANT_BULLET2);
        _bullet_3_image.LoadFromFile(VARIANT_BULLET3);
        _heart_image.LoadFromFile(VARIANT_HEART);

        scoreText.SetColor(sf::Color::White);
        cashText.SetColor(sf::Color::White);
    }

    //on enlève le lissage des images qu'on redimensionne
    _ship_player_image.SetSmooth(false);
    _ship_1_image.SetSmooth(false);
    _ship_2_image.SetSmooth(false);
    _ship_3_image.SetSmooth(false);
    _bullet_1_image.SetSmooth(false);
    _bullet_2_image.SetSmooth(false);
    _bullet_3_image.SetSmooth(false);
    _hpack_image.SetSmooth(false);
    _doublefire_image.SetSmooth(false);
    _firerate_image.SetSmooth(false);
    _heart_image.SetSmooth(false);
    _coin_image.SetSmooth(false);
    _bomb_bonus_image.SetSmooth(false);
    _bomb_image.SetSmooth(false);

    _background_sprite = Sprite(_background_image);
    _background_sprite.Resize(_w, _h);
    _background_sprite.SetPosition(0, 0);

    _background_sprite2 = Sprite(_background_image2);
    _background_sprite2.Resize(_w, _h);
    _background_sprite2.SetPosition(0, 0);

    _heart_sprite = Sprite(_heart_image);
    _heart_sprite.Resize(UI_IMAGE_SIZE, UI_IMAGE_SIZE);
    _bomb_sprite = Sprite(_bomb_image);
    _bomb_sprite.Resize(UI_IMAGE_SIZE, UI_IMAGE_SIZE);

    _thrust_fast_sprite = Sprite(_thrust_fast_image);
    _thrust_normal_sprite = Sprite(_thrust_normal_image);
    _thrust_slow_sprite = Sprite(_thrust_slow_image);

    _thrust_fast_sprite.Resize(Entity::DEF_W, Entity::DEF_H);
    _thrust_normal_sprite.Resize(Entity::DEF_W, Entity::DEF_H);
    _thrust_slow_sprite.Resize(Entity::DEF_W, Entity::DEF_H);
}

void GameView::setModel(GameModel * model)
{
    //récupération du model et mise à jour des collections
    _model = model;
    _enemies = _model->getEnemies();
    _bullets = _model->getBullets();
    _items = _model->getItems();
    _level = _model->getLevel();
    _player = _model->getPlayer();

    _elementToGraphicElement.clear();
}

void GameView::draw()
{
    _enemies = _model->getEnemies();
    _bullets = _model->getBullets();
    _items = _model->getItems();
    _level = _model->getLevel();
    _player = _model->getPlayer();

    _window->Clear();


    //si le fond se situe entre ]- largeur; 0], on le décale à gauche en en dessinant 2 à la suite
    if(_background_sprite.GetPosition().x <= 0 && _background_sprite.GetPosition().x > -_background_sprite.GetSize().x)
    {
        _background_sprite.SetPosition(_background_sprite.GetPosition().x -_player->getDX()/5, 0);
        _window->Draw(_background_sprite);
        _background_sprite.SetPosition(_background_sprite.GetPosition().x + _background_sprite.GetSize().x, 0);
        _window->Draw(_background_sprite);
        _background_sprite.SetPosition(_background_sprite.GetPosition().x - _background_sprite.GetSize().x, 0);
    }
    else // sinon si le x du fond est inférieur à - largeur (décallage maximum utile) on dessine le fond normalement en 0,0 et un autre à sa droite pour "boucler" l'image
    {
        _background_sprite.SetPosition(0, 0);
        _window->Draw(_background_sprite);
        _background_sprite.SetPosition(_background_sprite.GetSize().x, 0);
        _window->Draw(_background_sprite);
        _background_sprite.SetPosition(0, 0);
    }


    //selon la vitesse du joueur : sprite réacteur différent
    if(_player->getDX() == Player::FAST_SPEED)
        _window->Draw(_thrust_fast_sprite);
    else if (_player->getDX() == Player::SLOW_SPEED)
        _window->Draw(_thrust_slow_sprite);
    else
        _window->Draw(_thrust_normal_sprite);


    //on dessine les entités
    for(auto ge : _elementToGraphicElement)
        ge.second->draw(_model->getPlayer()->getX() - X_ORIGIN , _window);

    for(auto i : _itemToGraphicElement)
        i.second->draw(_model->getPlayer()->getX() - X_ORIGIN, _window);


    //on dessine la vie des ennemis
    for(Enemy * e : *_enemies)
        drawHealth(*e);

    //... l'interface
    drawInterface();

    _window->Display();
}

bool GameView::treatEvents()
{
    bool continueGame = true;

    if(_window->IsOpened())
    {

        //évenements clavier
        const Input& windowInputs = _window->GetInput();

        if(_window->GetInput().IsKeyDown(sf::Key::Z))
        {
            _player->setDY(- Ship::DEF_DY);
        }
        if(windowInputs.IsKeyDown(sf::Key::S))
        {
            _player->setDY(Ship::DEF_DY);
        }
        if(!windowInputs.IsKeyDown(sf::Key::S) && !windowInputs.IsKeyDown(sf::Key::Z))
        {
            _player->setDY(0);
        }


        if(windowInputs.IsKeyDown(sf::Key::Q))
        {
            _player->setDX(Player::SLOW_SPEED);
        }
        if(windowInputs.IsKeyDown(sf::Key::D))
        {
            _player->setDX(Player::FAST_SPEED);
        }
        if(!windowInputs.IsKeyDown(sf::Key::Q) && !windowInputs.IsKeyDown(sf::Key::D))
        {
            _player->setDX(Player::NORMAL_SPEED);
        }


        if(windowInputs.IsKeyDown(sf::Key::Return))
        {
            if(_player->canShoot())
            {
                if(_player->hasDoubleBullets())
                    _player->doubleShoot(_bullets);
                else
                    _bullets->insert(_player->shoot());
            }
        }

        if(windowInputs.IsKeyDown(sf::Key::Space) && !usedBomb)
        {
            _player->bomb(*_enemies, *_items);
            usedBomb = true;
        }

        if(!windowInputs.IsKeyDown(sf::Key::Space))
        {
            usedBomb = false;
        }

        if(windowInputs.IsKeyDown(sf::Key::Escape))
        {
            _game->setScreen(MenuScreen::menuMain(_window, _game));
        }

        if(windowInputs.IsKeyDown(sf::Key::T))
        {
            _player->upgradeDamage();
            _player->upgradeFireRate();
            _player->upgradeHealth();
        }


        Event event;
        _window->GetEvent(event);
        while(_window->GetEvent(event))
        {}
    }

    return continueGame;
}

void GameView::synchronize()
{
    //suppression des MovableEntity qui n'existent plus dans le model
    set<MovableEntity *> movableEntities = _model->getEntities();
    for(auto e = _elementToGraphicElement.begin(); e != _elementToGraphicElement.end(); e++)
    {
        if(movableEntities.find(e->first) == movableEntities.end())
        {
            delete e->second;
            _elementToGraphicElement.erase(e);
        }
    }

    //ajout des MovableEntity qui n'existent pas dans _elementToGraphicElement ou mise à jour des positions si ils existent
    for(MovableEntity * me : movableEntities)
    {
        if(_elementToGraphicElement.find(me) == _elementToGraphicElement.end())
        {
            if(dynamic_cast<Enemy *>(me) != nullptr)
            {
                Enemy * e = (Enemy*) me;
                if(e->getType() == 0)
                {
                    _elementToGraphicElement[me] = new GraphicElement(&_ship_1_image, me->getX(), me->getY(), me->getW(), me->getH(), 90);
                }
                else if(e->getType() == 1)
                {
                    _elementToGraphicElement[me] = new GraphicElement(&_ship_2_image, me->getX(), me->getY(), me->getW(), me->getH(), 90);
                }
                else
                {
                    _elementToGraphicElement[me] = new GraphicElement(&_ship_3_image, me->getX(), me->getY(), me->getW(), me->getH(), 90);
                }

            }

            else if(dynamic_cast<Bullet *>(me) != nullptr)
            {
                Bullet * b = dynamic_cast<Bullet *>(me);

                if(b->getEnemyType() == 0)
                {
                    _elementToGraphicElement[me] = new GraphicElement(&_bullet_1_image, me->getX(), me->getY(), me->getW(), me->getH(), 0);
                }
                else if(b->getEnemyType() == 1)
                {
                    _elementToGraphicElement[me] = new GraphicElement(&_bullet_2_image, me->getX(), me->getY(), me->getW(), me->getH(), 0);
                }
                else if(b->getEnemyType() == 2)
                {
                    _elementToGraphicElement[me] = new GraphicElement(&_bullet_3_image, me->getX(), me->getY(), me->getW(), me->getH(), 0);
                }
                else
                {
                    _elementToGraphicElement[me] = new GraphicElement(&_bullet_1_image, me->getX(), me->getY(), me->getW(), me->getH(), 0);
                }

            }

            else if(dynamic_cast<Player *>(me) != nullptr)
            {
                _elementToGraphicElement[me] = new GraphicElement(&_ship_player_image, me->getX(), me->getY(), me->getW(), me->getH(), -90);
            }
        }
        else
        {
            _elementToGraphicElement[me]->setPosition(me->getX(), me->getY());
        }
    }

    //suppression des Items (bonus) qui n'existent plus dans le model
    set<Item *> items = *(_model->getItems());
    for(auto i = _itemToGraphicElement.begin(); i != _itemToGraphicElement.end(); i++)
    {
        if(items.find(i->first) == items.end())
        {
            delete i->second;
            _itemToGraphicElement.erase(i);
        }
    }

    //ajout des bonus qui n'existent pas dans _itemToGraphicElement
    for(Item * i : items)
    {

        if(_itemToGraphicElement.find(i) == _itemToGraphicElement.end())
        {
            if(i->getType() == ItemType::HealthPack)
            {
                _itemToGraphicElement[i] = new GraphicElement(&_hpack_image, i->getX(), i->getY(), i->getW(), i->getH(), 0);
            }
            else if(i->getType() == ItemType::DoubleBullets)
            {
                _itemToGraphicElement[i] = new GraphicElement(&_doublefire_image, i->getX(), i->getY(), i->getW(), i->getH(), 0);
            }
            else if(i->getType() == ItemType::FireRate)
            {
                _itemToGraphicElement[i] = new GraphicElement(&_firerate_image, i->getX(), i->getY(), i->getW(), i->getH(), 0);
            }
            else if(i->getType() == ItemType::Coin)
            {
                _itemToGraphicElement[i] = new GraphicElement(&_coin_image, i->getX(), i->getY(), i->getW(), i->getH(), 0);
            }
            else if(i->getType() == ItemType::Bomb)
            {
                _itemToGraphicElement[i] = new GraphicElement(&_bomb_bonus_image, i->getX(), i->getY(), i->getW(), i->getH(), 0);
            }

        }

    }

    //mise à jour de la position des sprites réacteur qui doivent suivre le joueur
    _thrust_fast_sprite.SetPosition(X_ORIGIN - Entity::DEF_W + 7, _player->getY());
    _thrust_normal_sprite.SetPosition(X_ORIGIN - Entity::DEF_W + 7, _player->getY());
    _thrust_slow_sprite.SetPosition(X_ORIGIN - Entity::DEF_W + 7, _player->getY());
}

void GameView::drawHealth(const Ship & ship)
{
    //création rectangle vie vaisseau
    Shape borders = sf::Shape::Rectangle(ship.getX() + X_ORIGIN - _player->getX(), ship.getY() + ship.getH(), ship.getX() + ship.getW() - _player->getX() + X_ORIGIN , ship.getY() + ship.getH() + 10, sf::Color::White, true, sf::Color::Red);
    Shape hp = sf::Shape::Rectangle(ship.getX() + X_ORIGIN - _player->getX(), ship.getY() + ship.getH(), ship.getX() + (ship.getW() * ship.getHP() / ship.getMaxHealth()) - _player->getX() + X_ORIGIN , ship.getY() + ship.getH() + 10, sf::Color::Red, false);
    _window->Draw(borders);
    _window->Draw(hp);
}

void GameView::drawInterface()
{
    //création rectangle vie joueur
    int hpWidth = 150;
    Shape borders = sf::Shape::Rectangle(20, 20, hpWidth, 50, sf::Color::White, true, sf::Color::Red);
    Shape hp = sf::Shape::Rectangle(20, 20, hpWidth * _player->getHP() / _player->getMaxHealth(), 50, sf::Color::Red, false);

    scoreText.SetText("SCORE : " + to_string(_player->getPoints()));
    cashText.SetText("CASH : " + to_string(_player->getCash()) + "$");

    _window->Draw(borders);
    _window->Draw(hp);
    _window->Draw(scoreText);
    _window->Draw(cashText);

    //dessine le nb de vies
    for(int i = 0; i < _player->getLife(); i++)
    {
        _heart_sprite.SetPosition( hpWidth + 20 + (UI_IMAGE_SIZE + 10 ) * i, 20);
        _window->Draw(_heart_sprite);
    }

    //dessine le nb de bombes
    for(int i = 0; i < _player->getBombsNumber(); i++)
    {
        _bomb_sprite.SetPosition( 20 + (UI_IMAGE_SIZE + 10 ) * i, 70);
        _window->Draw(_bomb_sprite);
    }

}
