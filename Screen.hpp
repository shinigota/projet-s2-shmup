#ifndef SCREEN_H
#define SCREEN_H

#include<SFML/Graphics.hpp>

class Screen
{
 public:
    virtual ~Screen() {};

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return: /
    // Purpose: drawing the screen
    //------------------------------------------------------------------------------
    virtual void draw()=0;

    //------------------------------------------------------------------------------
    // Input: float fps the number of frames per second
    // Output:  /
    // Return: /
    // Purpose: update the screen with the number of fps so that the screens keeps updating at the same speed all the time
    //------------------------------------------------------------------------------
    virtual void update(float fps)=0;
};

#endif // SCREEN_H
