#ifndef _MENUSCREEN_HPP_
#define _MENUSCREEN_HPP_

#include <set>
#include "Button.hpp"
#include "Screen.hpp"
#include "Game.hpp"
#include <SFML/Graphics.hpp>



class MenuScreen : public Screen{

protected:
    Game * _game;
    std::set<Button *> _buttons;
    sf::RenderWindow * _window;
    sf::Font _textFont;
    sf::String _title;

    MenuScreen(sf::RenderWindow * window, Game * game);
    void addButton(Button * b);
    void addText(std::string b);


public:

    virtual ~MenuScreen();

    //------------------------------------------------------------------------------
    // Input: a window and a game
    // Output:  /
    // Return: return a pointer on MenuScreen
    // Purpose: configure a MenuScreen of options with differents buttons
    //------------------------------------------------------------------------------
    static MenuScreen * menuOptions(sf::RenderWindow * window, Game * game);

    //------------------------------------------------------------------------------
    // Input: a window and a game
    // Output:  /
    // Return:  return a pointer on MenuScreen
    // Purpose: configure a MenuScreen with button usefull for build a main menu
    //------------------------------------------------------------------------------
    static MenuScreen * menuMain(sf::RenderWindow * window, Game * game);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return: /
    // Purpose: Draw Menu's Buttons and text of Menu
    //------------------------------------------------------------------------------
    virtual void draw();

    //------------------------------------------------------------------------------
    // Input: fps, the number of frame per second the screen will use to update (same update speed with fps)
    // Output:  /
    // Return:
    // Purpose: Update menu when an event is detected
    //------------------------------------------------------------------------------
    virtual void update(float fps);


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return: _buttons
    // Purpose: Getter on _buttons
    //------------------------------------------------------------------------------
    std::set<Button *> getButtons();

};

#endif // _MENUSCREEN_HPP_
