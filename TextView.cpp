#include "TextView.hpp"
#include "GameModel.hpp"
#include "Enemy.hpp"
#include "Bullet.hpp"
#include "Item.hpp"
#include "Level.hpp"
#include "Player.hpp"
#include <iostream>

using namespace std;

TextView::TextView()
    :_w(TextView::DEF_SCREEN_W), _h(TextView::DEF_SCREEN_H)
{}

TextView::TextView(unsigned int w, unsigned int h)
    :_w(w), _h(h)
{}

TextView::~TextView()
{}

void TextView::setModel(GameModel * model)
{
    _model = model;
    _enemies = _model->getEnemies();
    _bullets = _model->getBullets();
    _items = _model->getItems();
    _level = _model->getLevel();
    _player = _model->getPlayer();

}

void TextView::draw()
{
    _enemies = _model->getEnemies();
    _bullets = _model->getBullets();
    _items = _model->getItems();
    _level = _model->getLevel();
    _player = _model->getPlayer();

     system("clear");
    cout << "==== New step ====" << endl;

    cout << _level->toString() << endl;
    cout << _player->toString() << endl;

    for(Enemy * e : * _enemies)
        cout << e->toString() << endl;

    for(Bullet * b : * _bullets)
        cout << b->toString() << endl;

    for(Item * i : * _items)
        cout << i->toString() << endl;

    if(_model->gameEnded())
        cout << "Game ended, no more life" << endl;
}

bool TextView::treatEvents(int fps)
{
    bool continueGame = true;
    char val;

    cout << "--------------------------------------------------- Controls ---------------------------------------------------" << endl;
    cout << "z : move up | s : move down | d : move left faster | q : stop moving left | a : use bomb | e : shoot | x : quit " << endl;

    cin >> val;

    switch(val)
    {
        case 'z':
            _player->setDY(- Ship::DEF_DY);
            _player->setDX(Ship::DEF_DX);
            break;

        case 's':
            _player->setDY(Ship::DEF_DY);
            _player->setDX(Ship::DEF_DX);
            break;

        case 'd':
            _player->setDX(2 * Ship::DEF_DX);
            _player->setDY(0);
            break;

        case 'q':
            _player->setDX(0);
            _player->setDY(0);
            break;
        case 'a':
            _player->bomb(*_enemies, *_items);
            break;
        case 'e':
            if(_player->canShoot())
            {
                if(_player->hasDoubleBullets())
                    _player->doubleShoot(_bullets);
                else
                    _bullets->insert(_player->shoot());
            }

            break;
        case 'x':
            continueGame = false;
            break;
       default:
            _player->setDX(Ship::DEF_DX);
            _player->setDY(0);
            break;

    }

    return continueGame;
}
