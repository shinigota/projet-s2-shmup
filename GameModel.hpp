#ifndef GAMEMODEL_HPP
#define GAMEMODEL_HPP

#include <set>
#include <ctime>

class Enemy;
class Player;
class Bullet;
class Item;
class Level;
class MovableEntity;
class Game;

class GameModel {
private:
    int _w, _h,_seed, _currentDifficulty;
    Game * _game;
    Level * _level;
    std::set<Enemy*> * _enemies;
    std::set<Bullet*> _bullets;
    std::set<Item*> _items;
    Player * _player;
    long _lastUpdate;
    int _fps;

    void clearAll();

public:
    GameModel(Game * game);
    ~GameModel();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: updating the game engine step by step
    //------------------------------------------------------------------------------
    void nextStep(int fps);

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  bool true if there's no more enemy left or if the player is dead , else false
    // Purpose: checking if the model has to stop or no
    //------------------------------------------------------------------------------
    bool gameEnded();

    //Getters
    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  pointer of set of enemies
    // Purpose: having a pointer of set of enemies
    //------------------------------------------------------------------------------
    std::set<Enemy*> * getEnemies();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  pointer of set of bullets
    // Purpose: having a pointer of the set of bullets
    //------------------------------------------------------------------------------
    std::set<Bullet*> * getBullets();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  pointer of set of items
    // Purpose: having a pointer of the set of items
    //------------------------------------------------------------------------------
    std::set<Item*> * getItems();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  pointer of the current level
    // Purpose: having a pointer of the current level
    //------------------------------------------------------------------------------
    Level * getLevel();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  pointer of the player
    // Purpose: having a pointer of the player
    //------------------------------------------------------------------------------
    Player * getPlayer();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  a set of all the movable entities
    // Purpose: used to get all the movable entities in one set (for the gameview, synchronize method)
    //------------------------------------------------------------------------------
    std::set<MovableEntity *> getEntities();
};

#endif // GAMEMODEL_HPP
