#ifndef TEXTVIEW_HPP
#define TEXTVIEW_HPP

#include <set>

class GameModel;
class Enemy;
class Bullet;
class Item;
class Level;
class Player;
class GraphicElement;
class MovableEntity;
class Ship;

class TextView
{
private:
    GameModel * _model;
    unsigned int _w, _h;
    std::set<Enemy *> * _enemies;
    std::set<Bullet *> * _bullets;
    std::set<Item *> * _items;
    Level * _level;
    Player * _player;

public:
    static const unsigned int DEF_SCREEN_H=800, DEF_SCREEN_W=600;
    TextView();
    TextView(unsigned int w, unsigned int h);
    ~TextView();


    //------------------------------------------------------------------------------
    // Input: model, the model to link with the view
    // Output:  /
    // Return:  /
    // Purpose: linking model and view
    //------------------------------------------------------------------------------
    void setModel(GameModel * model);


    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  /
    // Purpose: rendering entities in text format
    //------------------------------------------------------------------------------
    void draw();

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  false if quit event, else true
    // Purpose: treating events and knowing if view still has to treat them
    //------------------------------------------------------------------------------
    bool treatEvents(int fps);
};

#endif // TEXTVIEW_HPP


