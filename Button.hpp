#ifndef BUTTON_HPP
#define BUTTON_HPP

#include <SFML/Graphics.hpp>
#include <iostream>
#include "Player.hpp"

class Game;


class Button
{
private:
    int _x,_y;
    unsigned int _w,_h;
    sf::Image _img;
    sf::Image _imgHover;
    sf::Sprite _sprite;
    sf::Sprite _sprite_hover;
    bool _hover;
    std::function<void(Game *)> _action;

    void setImages(std::string normalPath, std::string hoverPath);
    Button(int x, int y,unsigned int w,unsigned int h, std::string path, std::string pathHover, std::function<void(Game *)> action);
    Button(int x, int y,unsigned int w,unsigned int h, std::string path, std::string pathHover);

    void setAction(std::function<void(Game *)> action);

public:
    static const unsigned int DEF_WIDTH = 200, DEF_HEIGHT = 70;

    ~Button();

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Button allow to begin a game
    //------------------------------------------------------------------------------
    static Button * ButtonStart(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Button allow to return at game after ShopScreen
    //------------------------------------------------------------------------------
    static Button * ButtonContinue(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Button allow to access to option menu
    //------------------------------------------------------------------------------
    static Button * ButtonOption(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Button that sendto credits screen
    //------------------------------------------------------------------------------
    static Button * ButtonCredits(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Quit the game
    //-----------------------------------------------------------------------------
    static Button * ButtonQuit(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Button allow to change graphics of game
    //------------------------------------------------------------------------------
    static Button * ButtonGraphics(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Button allow to come back to main menu
    //------------------------------------------------------------------------------
    static Button * ButtonMainMenu(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Button allow to upgrade damage of bullet
    //------------------------------------------------------------------------------
    static Button * ButtonDamageUp(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Button allow to upgrade fire speed
    //------------------------------------------------------------------------------
    static Button * ButtonUpFireSpeed(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Button allow to upgrade healthlevel
    //------------------------------------------------------------------------------
    static Button * ButtonLifeUp(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Button allow to upgrade Bomb power
    //------------------------------------------------------------------------------
    static Button * ButtonUpBombPower(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);

    //------------------------------------------------------------------------------
    // Input: position (x and y), size (w and h), 2 paths on Image of button(normal and hover)
    // Output:  /
    // Return:  pointer on Button
    // Purpose: Button allow to buy bomb
    //------------------------------------------------------------------------------
    static Button * ButtonBuyBomb(int x, int y, unsigned int w, unsigned int h, std::string path, std::string pathHover);


    //------------------------------------------------------------------------------
    // Input: position (x and y)
    // Output:  /
    // Return:  true if position is include in button, else false
    // Purpose: make a test if button contain position x and y
    //------------------------------------------------------------------------------
    bool contains(int x, int y) const;

    //------------------------------------------------------------------------------
    // Input: pointer on window
    // Output:  /
    // Return:  /
    // Purpose: draw button in window
    //------------------------------------------------------------------------------
    void draw(sf::RenderWindow * window);

    //------------------------------------------------------------------------------
    // Input: pointer on game
    // Output:  /
    // Return:  /
    // Purpose: act the function of button
    //------------------------------------------------------------------------------
    void act(Game * g) const;


    //Getters
    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  x position of the button
    // Purpose: knowing the x position of the button
    //------------------------------------------------------------------------------
    int getX() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  y position of the button
    // Purpose: knowing the y position of the button
    //------------------------------------------------------------------------------
    int getY() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  width of the button
    // Purpose: knowing the width of the button
    //------------------------------------------------------------------------------
    unsigned int getW() const;

    //------------------------------------------------------------------------------
    // Input: /
    // Output:  /
    // Return:  height of the button
    // Purpose: knowing the height of the button
    //------------------------------------------------------------------------------
    unsigned int getH() const;

    //Setter
    //------------------------------------------------------------------------------
    // Input: bool : the new value
    // Output:  /
    // Return:  /
    // Purpose: change hover state of the button
    //------------------------------------------------------------------------------
    void setHover(bool hover);

};


#endif //BUTTON_HPP
