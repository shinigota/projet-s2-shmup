#include "GameScreen.hpp"

GameScreen::GameScreen(sf::RenderWindow * window, Game * game, bool defaultGraphics)
    :_window(window), _model(GameModel(game)), _view(game, defaultGraphics, &_model), _game(game)
{}

GameScreen::~GameScreen()
{

}

void GameScreen::draw()
{
    _view.draw();
}

void GameScreen::update(float fps)
{
    _model.nextStep(fps);
    _view.treatEvents();
    _view.synchronize();
}

Player * GameScreen::getPlayer()
{
    return _model.getPlayer();
}

void GameScreen::reloadGraphics()
{
  //  _view.initSprites();
}
